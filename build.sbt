//=============================================================================
//Start of file 'build.sbt'
//=============================================================================
lazy val myScalaVersion =  "2.13.14"
lazy val programName  = "fitsVerifier"
lazy val authorList   = "Rafael Morales (rmorales@iaa.es)"
lazy val license      = "This project is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)"
//-----------------------------------------------------------------------------
lazy val commonKeys = Seq(
    name              := programName
  , scalaVersion      := myScalaVersion
  , description       := s"$programName. Implementation of FITS standard version 4.0 (2016 July 22)"
  , organization      := "IAA-CSIC"
  , homepage          :=  Some(url("http://www.iaa.csic.es"))
)
//-----------------------------------------------------------------------------
lazy val mainClassName = s"com.fitsVerifier.Main"
//-----------------------------------------------------------------------------
//https://github.com/benblack86/finest
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq(
  "-Xlint",
  "-deprecation",
  "-feature",
  "-unchecked"
)
//-----------------------------------------------------------------------------
//versions
val apacheCommonsFileUtils   = "2.16.1"
val apacheCommonsCsvVersion  = "1.11.0"
val apacheCommonsMath        = "3.6.1"
val apacheSparkVersion       = "2.4.4"
val apacheLogCoreVersion     = "2.19.0"
val apacheLogScalaVersion    = "13.1.0"
val commandLineParserVersion = "5.1.0"
val fastParseVersion         = "3.1.1"
val jamaVersion              = "1.0.3"
val jniLibraryVersion        = "1.7.1"
val qosLogbackVersion        = "1.5.8"
val scalaReflectVersion      = myScalaVersion
val scalaTestVersion         = "3.3.0-SNAP3"
val typeSafeVersion          = "1.4.3"
//-----------------------------------------------------------------------------
//assembly
assembly / mainClass := Some(mainClassName)
assembly / logLevel  := Level.Warn

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}
//-----------------------------------------------------------------------------
//tests
test / parallelExecution := false
//-----------------------------------------------------------------------------
//exclude directories form source path: github.com/sbt/sbt-jshint/issues/14
unmanagedSources / excludeFilter := {
    new SimpleFileFilter(_.getCanonicalPath contains "compression/lz4") ||
    new SimpleFileFilter(_.getCanonicalPath contains "compression/xz") ||
    new SimpleFileFilter(_.getCanonicalPath contains "coordinate/spherical") ||
    new SimpleFileFilter(_.getCanonicalPath contains "fits/metaData") ||
    new SimpleFileFilter(_.getCanonicalPath contains "fits/synthetize") ||
    new SimpleFileFilter(_.getCanonicalPath contains "fits/wcs/fit") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/astrometry") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/catalog") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/estimator/flux") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/estimator/skyPosition") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/focusType") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/fumo/") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/mpo") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/myImage/dir") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/nsaImage") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/occultation") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/photometry") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/registration") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/script") ||
    new SimpleFileFilter(_.getCanonicalPath contains "image/web") ||
    new SimpleFileFilter(_.getCanonicalPath contains "stream/remote") ||
    new SimpleFileFilter(_.getCanonicalPath contains "myJava/nom/tam") ||
    new SimpleFileFilter(_.getCanonicalPath contains "myJava/wcs") ||
    new SimpleFileFilter(_.getCanonicalPath contains "util/computerInfo") ||
    new SimpleFileFilter(_.getCanonicalPath contains "util/mail") ||
    new SimpleFileFilter(_.getCanonicalPath contains "util/parser")
}
//-----------------------------------------------------------------------------
//resolvers
resolvers ++= Resolver.sonatypeOssRepos("public")

//-----------------------------------------------------------------------------
//git versioning: https://github.com/sbt/sbt-git

git.useGitDescribe := true
git.uncommittedSignifier := None         //do not append suffix is some changes are not pushed to git repo
git.gitDescribePatterns := Seq("v*.*")   //format of the tag version

//https://stackoverflow.com/questions/56588228/how-to-get-git-gittagtoversionnumber-value
git.gitTagToVersionNumber := { tag: String =>
  if (tag.isEmpty)  None
  else Some(tag.split("-").toList.head)
}

//See configuration options in: https://github.com/sbt/sbt-buildinfo

//package and name of the scala code auto-generated
buildInfoPackage := "BuildInfo"
buildInfoObject := "BuildInfo"

//append build time
buildInfoOptions += BuildInfoOption.BuildTime

//append extra options to be generated
buildInfoKeys ++= Seq[BuildInfoKey](
    buildInfoBuildNumber
  , scalaVersion
  , sbtVersion
  , name
  , version
  , "authorList" -> authorList
  , "license" -> license
  , description
  , organization
  , homepage
  , BuildInfoKey.action("myGitCurrentBranch") {git.gitCurrentBranch.value}
  , BuildInfoKey.action("myGitHeadCommit")    {git.gitHeadCommit.value.get}
)

//The auto generated scala code with ths info is located at: target/scala-2.12/src_managed/main/sbt-buildinfo/BuildInfo.scala
//The code can be used in the code with 'import buildinfo.BuildInfo'
//=============================================================================
//dependence list
lazy val dependenceList = Seq(

  //manage configuration files. https://github.com/typesafehub/config
  "com.typesafe" % "config" % typeSafeVersion

  //logging: https://logging.apache.org/log4j/2.x/manual/scala-api.html
  , "org.apache.logging.log4j" %% "log4j-api-scala" % apacheLogScalaVersion
  , "org.apache.logging.log4j" % "log4j-core" % apacheLogCoreVersion % Runtime
  , "ch.qos.logback" % "logback-classic" % qosLogbackVersion

  //command line argument parserTform: https://github.com/scallop/scallop
  , "org.rogach" %% "scallop" % commandLineParserVersion

  //apache commons file utils
  , "commons-io" % "commons-io" % apacheCommonsFileUtils

  //apache commons; CSV management
  , "org.apache.commons" % "commons-csv" % apacheCommonsCsvVersion

  //https://mvnrepository.com/artifact/org.apache.commons/commons-math3
  , "org.apache.commons" % "commons-math3" % apacheCommonsMath

  //grammar parserTform: https://github.com/com-lihaoyi/fastparse
  , "com.lihaoyi" %% "fastparse" % fastParseVersion

  //scala reflect
  , "org.scala-lang" % "scala-reflect" % scalaReflectVersion

  //jni: https://mvnrepository.com/artifact/com.github.sbt/sbt-jni-core
  // remember to append 'addSbtPlugin("com.github.sbt" % "sbt-jni" % "jniLibraryVersion")' in plugin.sbt
  , "com.github.sbt" %% "sbt-jni-core" % jniLibraryVersion

  // basic linear algebra package for Java.  https://mvnrepository.com/artifact/gov.nist.math/jama
  , "gov.nist.math" % "jama" % jamaVersion

  //scala test
  , "org.scalatest" %% "scalatest" % scalaTestVersion % Test
)
//=============================================================================
//root project
lazy val root = (project in file("."))
  .enablePlugins(BuildInfoPlugin, GitVersioning)
  .settings(libraryDependencies ++= dependenceList)
  .settings(commonKeys: _*)
//-----------------------------------------------------------------------------
//End of file 'build.sbt'
//-----------------------------------------------------------------------------
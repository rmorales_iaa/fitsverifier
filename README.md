Implementation of FITS standard version 4.0: (2016 July 22)
Includes:

    a) Verification of physical units strings

        java -jar fitsVerifier.jar -u "10^(46)sqrt(a/s*m/(log(a^(3/2))/log(s*m))/log(s))" -m
        
    b) Verification of FITS structure: Primary HDU, extensions (incluing 'random groups') and 'special records'

    c) Dump the contents of each FITS structure


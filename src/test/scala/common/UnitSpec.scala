/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/Feb/2021
 * Time:  11h:10m
 * Description: None
 */
//=============================================================================
package common
//=============================================================================
//=============================================================================
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.must.Matchers
import org.scalatest.{Inside, Inspectors, OptionValues}
//=============================================================================
//See http://www.scalatest.org/user_guide/defining_base_classes
abstract class UnitSpec extends AsyncFlatSpec
  with Matchers
  with OptionValues
  with Inside
  with Inspectors

//=============================================================================
//=============================================================================
//End of file UnitSpec.scala
//=============================================================================

/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  28/mar/2018
  * Time:  11h:41m
  * Description: Unit testing for FITS parserTform of keyword + values records
  * The parserTform implements the rules of the grammar defined in appendix A of FITS standard version 4.0 (2018 August 13)
  * For a faster testing, please use a ramdisk with 10GiB and change the variable 'SPEC_DATA_OUT_DIR' to point to it
  */
//=============================================================================
package standard
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.standard.dataType.DataType._
import com.common.fits.standard.disconformity.Disconformity
import com.common.fits.standard.fits.{Fits, FitsLoad}
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.Extension.EXTENSION_CONFORMING_DUMP_NAME
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.TableColBuild
import com.common.util.file.MyFile
import com.common.util.path.Path
import com.common.util.util.Util
import com.common.fits.standard.fits.factory.FitsFactory
import standard.CommonSpec._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform._
//=============================================================================
import common.UnitSpec
//=============================================================================
object Section_3_1_spec{
  //---------------------------------------------------------------------------
  private val SECTION_NAME = SectionName.SECTION_3_1
  private val NORMALIZED_PATH = Path.ensureEndWithFileSeparator(SPEC_DATA_OUT_DIR + normalizeSectionName(SECTION_NAME))
  //---------------------------------------------------------------------------
  //List of test case
  val TEST_0O = SECTION_NAME + "-Test 00"
  val TEST_01 = SECTION_NAME + "-Test 01"
  val TEST_02 = SECTION_NAME + "-Test 02"
  val TEST_03 = SECTION_NAME + "-Test 03"
  val TEST_04 = SECTION_NAME + "-Test 04"
  val TEST_05 = SECTION_NAME + "-Test 05"
  val TEST_06 = SECTION_NAME + "-Test 06"
  val TEST_07 = SECTION_NAME + "-Test 07"
  val TEST_08 = SECTION_NAME + "-Test 08"
  val TEST_09 = SECTION_NAME + "-Test 09"
  val TEST_10 = SECTION_NAME + "-Test 10"
  val TEST_11 = SECTION_NAME + "-Test 11"
  val TEST_12 = SECTION_NAME + "-Test 12"
  val TEST_13 = SECTION_NAME + "-Test 13"
  val TEST_14 = SECTION_NAME + "-Test 14"
  //---------------------------------------------------------------------------
}
//=============================================================================
import Section_3_1_spec._
class Section_3_1_spec extends UnitSpec {
    //---------------------------------------------------------------------------
    TEST_0O must "have a valid structure sort with one primary with zero data" in {
      initialActions(NORMALIZED_PATH)
      val fitsName = NORMALIZED_PATH + "0.fits"

      FitsFactory.primary(
          fitsName
        , bitPix = DATA_TYPE_INT_16.bitSize
        , axisSeq = Array(0, 0))

      val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
      assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")
      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }
    //---------------------------------------------------------------------------
    //data too large,use only in command line test, not inside IDE

    TEST_01 + "-Test 1" must "have a valid structure sort with one primary with 8GiB data" in {
      initialActions(NORMALIZED_PATH)
      val fitsName = NORMALIZED_PATH + "1.fits"

      FitsFactory.primary(
        fitsName
        , bitPix = DATA_TYPE_INT_16.bitSize
        //, axisSeq = Array(65536, 65536))
        , axisSeq = Array(6553, 6553))

      val optFits = FitsLoad.load(fitsName, readDataBlockFlag = false)
      assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")

      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }

    //---------------------------------------------------------------------------
    TEST_02 must "have a valid structure sort with: primary and extension" in {
      initialActions(NORMALIZED_PATH)
      val fitsName = NORMALIZED_PATH + "2.fits"

      FitsFactory.extension(
        fitsName
        , mainHduBitPix = DATA_TYPE_INT_32.bitSize
        , mainHduAxisSeq = Array(8, 80)
        , extName = EXTENSION_CONFORMING_DUMP_NAME
        , extBitPix = 8
        , extensionAxisSeq = Array(1024)
        , pcount = 0
        , gcount = 1)

      val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
      assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")

      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }
    //---------------------------------------------------------------------------
    TEST_03 must "have a valid structure sort with: primary and special record" in {
      initialActions(NORMALIZED_PATH)
      val fitsName = NORMALIZED_PATH + "3.fits"

      FitsFactory.specialRecord(
          fitsName
        , bitPix = DATA_TYPE_U_INT_8.bitSize
        , axisSeq = Array(1)
        , specialByteSeq = "SPECIAL RECORD".getBytes())

      val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
      assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")

      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }
    //---------------------------------------------------------------------------
    TEST_04 must "have a valid structure with: primary, extension and special record" in {
      initialActions(NORMALIZED_PATH)
      val fitsName = NORMALIZED_PATH + "4.fits"

      val fitsSpecial = FitsFactory.specialRecord(
        fitsName
        , bitPix = DATA_TYPE_U_INT_8.bitSize
        , axisSeq = Array(1)
        , specialByteSeq = "SPECIAL RECORD".getBytes()
        , saveFile = false)

        FitsFactory.extension(
          fitsName
        , mainHduBitPix = DATA_TYPE_INT_32.bitSize
        , mainHduAxisSeq = Array(8, 80)
        , extName = EXTENSION_CONFORMING_DUMP_NAME
        , extBitPix = 8
        , extensionAxisSeq = Array(1024)
        , pcount = 0
        , gcount = 1
        , additionalStructureSeq = Array(fitsSpecial.structureSeq.last))

      val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
      assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")
      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }
    //---------------------------------------------------------------------------
    TEST_05 must "have a valid structure with: primary, extension and extension" in {
      initialActions(NORMALIZED_PATH)
      val fitsName = NORMALIZED_PATH + "5.fits"

      val fitsHDU_Extension = FitsFactory.extension(
        fitsName
        , mainHduBitPix = DATA_TYPE_INT_32.bitSize
        , mainHduAxisSeq = Array(8, 80)
        , extName = EXTENSION_CONFORMING_DUMP_NAME
        , extBitPix = DATA_TYPE_U_INT_8.bitSize
        , extensionAxisSeq = Array(1024)
        , pcount = 0
        , gcount = 1
        , saveFile = false).get

      val structSeq = fitsHDU_Extension.structureSeq ++ fitsHDU_Extension.structureSeq.drop(1)
      Fits(fitsName, structSeq).saveLocal(fitsName)

      val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
      optFits.get.printDetailedInfo()
      assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")
      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }
    //---------------------------------------------------------------------------
    TEST_06 must "have a invalid structure with: extension,special record" in {
      MyConf.c = MyConf()
      val fitsName = NORMALIZED_PATH + "6.fits"

      val fitsSpecial = FitsFactory.specialRecord(
        fitsName
        , bitPix = DATA_TYPE_FLOAT_32.bitSize
        , axisSeq = Array(1,232)
        , specialByteSeq = "SPECIAL RECORD".getBytes()
        , saveFile = false)

      val fitsHDU_Extension = FitsFactory.extension(
        fitsName
        , mainHduBitPix = DATA_TYPE_FLOAT_64.bitSize
        , mainHduAxisSeq = Array(8, 80)
        , extName = EXTENSION_CONFORMING_DUMP_NAME
        , extBitPix = 8
        , extensionAxisSeq = Array(1024)
        , pcount = 0
        , gcount = 1
        , additionalStructureSeq = Array(fitsSpecial.structureSeq.last)).get

      Fits(fitsName, fitsHDU_Extension.structureSeq.drop(1)).saveLocal(fitsName)

      val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
      assert(optFits.isEmpty, s"The FITS file '$fitsName' must be not created")
      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }
    //---------------------------------------------------------------------------
    TEST_07 must "have a invalid structure sort with: special record" in {
      MyConf.c = MyConf()
      val fitsName = NORMALIZED_PATH + "7.fits"

      val fitsSpecial = FitsFactory.specialRecord(
        fitsName
        , bitPix = DATA_TYPE_FLOAT_32.bitSize
        , axisSeq = Array(1, 232)
        , specialByteSeq = "SPECIAL RECORD".getBytes()
        , saveFile = false)

      Fits(fitsName, fitsSpecial.structureSeq.drop(1)).saveLocal(fitsName)

      val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
      assert(optFits.isEmpty, s"The FITS file '$fitsName' must be not created")
      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }
    //---------------------------------------------------------------------------
    TEST_08 must "have a valid primary but not integral number of FITS blocks (645 bytes)" in {
      MyConf.c = MyConf()
      val fitsName = SPEC_DATA_IN_DIR + "/bad_block_0_5.fits"
      val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
      assert(optFits.isEmpty, s"The FITS file '$fitsName' must be not created")
    }
    //---------------------------------------------------------------------------
    TEST_09 must "have a valid primary but not integral number of FITS blocks (3704 bytes)" in {
      MyConf.c = MyConf()
      val fitsName = SPEC_DATA_IN_DIR + "/bad_block_1_5.fits"
      val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
      assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")
    }
    //---------------------------------------------------------------------------
    TEST_10 must "have a valid structure with: random group with data" in {
      initialActions(NORMALIZED_PATH)
      val fitsName = NORMALIZED_PATH + "/random_group_1.fits"

      val dataType = DATA_TYPE_INT_32
      val groupCount = 6
      val axisSeq = Array[Long](2,3,5) //starting with NAXIS2
      val randoGroupParameterNameSeq = Array("param_1", "param_2", "param_1")
      val randomGroupParameterValueSeq = Array(dataType.getByteSeq(3), dataType.getByteSeq(4), dataType.getByteSeq(99))
      val randomGroupDataSeq = Util.buildConsecutiveByteSeqSeq(groupCount, axisSeq.product * dataType.byteSize)

      FitsFactory.randomGroup(
        fitsName
        , mainHduBitPix = dataType.bitSize
        , mainHduAxisSeq = 0L +: axisSeq
        , randoGroupParameterNameSeq = randoGroupParameterNameSeq
        , randomGroupParameterValueSeq = randomGroupParameterValueSeq
        , randomGroupDataSeq = randomGroupDataSeq
      )

      val optFits = FitsLoad.load(fitsName)
      assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")

      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }
    //---------------------------------------------------------------------------
    TEST_11 must "have a valid structure with: random group with no groups" in {
      initialActions(NORMALIZED_PATH)
      val fitsName = NORMALIZED_PATH + "/random_group_2.fits"

      val dataType = DATA_TYPE_INT_32
      val groupCount = 0
      val axisSeq = Array[Long](2, 3, 5) //starting with NAXIS2
      val randoGroupParameterNameSeq = Array("param_1", "param_2", "param_1")
      val randomGroupParameterValueSeq = Array(dataType.getByteSeq(3), dataType.getByteSeq(4), dataType.getByteSeq(99))
      val randomGroupDataSeq = Util.buildConsecutiveByteSeqSeq(groupCount, axisSeq.product * dataType.byteSize)

      FitsFactory.randomGroup(
        fitsName
        , mainHduBitPix = dataType.bitSize
        , mainHduAxisSeq = 0L +: axisSeq
        , randoGroupParameterNameSeq = randoGroupParameterNameSeq
        , randomGroupParameterValueSeq = randomGroupParameterValueSeq
        , randomGroupDataSeq = randomGroupDataSeq
      )

      val optFits = FitsLoad.load(fitsName)
      assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")

      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }
    //---------------------------------------------------------------------------
    TEST_12 must "have a valid structure with: random group and extension" in {
      initialActions(NORMALIZED_PATH)
      val fitsName = NORMALIZED_PATH + "/random_group_3.fits"

      val fitsHDU_Extension = FitsFactory.extension(
        fitsName
        , mainHduBitPix = DATA_TYPE_INT_32.bitSize
        , mainHduAxisSeq = Array(8, 80)
        , extName = EXTENSION_CONFORMING_DUMP_NAME
        , extBitPix = DATA_TYPE_U_INT_8.bitSize
        , extensionAxisSeq = Array(1024)
        , pcount = 0
        , gcount = 1
        , saveFile = false).get

      val dataType = DATA_TYPE_INT_32
      val groupCount = 1002
      val axisSeq = Array[Long](2, 3, 5) //starting with NAXIS2
      val randoGroupParameterNameSeq = Array("param_1", "param_2", "param_1")
      val randomGroupParameterValueSeq = Array(dataType.getByteSeq(3), dataType.getByteSeq(4), dataType.getByteSeq(99))
      val randomGroupDataSeq = Util.buildConsecutiveByteSeqSeq(groupCount, axisSeq.product * dataType.byteSize)

      FitsFactory.randomGroup(
        fitsName
        , mainHduBitPix = dataType.bitSize
        , mainHduAxisSeq = 0L +: axisSeq
        , randoGroupParameterNameSeq = randoGroupParameterNameSeq
        , randomGroupParameterValueSeq = randomGroupParameterValueSeq
        , randomGroupDataSeq = randomGroupDataSeq
        , additionalStructureSeq = fitsHDU_Extension.structureSeq.drop(1)
      )

      val optFits = FitsLoad.load(fitsName)
      assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")

      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }
  //---------------------------------------------------------------------------
  TEST_13 must "have a valid structure with: ASCII table" in {
    initialActions(NORMALIZED_PATH)
    val fitsName = NORMALIZED_PATH + "/ascii_table_1.fits"

    val simpleColDefSeq = Array(
        TableColBuild(form  = AsciiTableTformFloatFixed(8,3), name = Some("COL_1"))
      , TableColBuild(form =  AsciiTableTformCharacter(1), name = Some("COL_2"))
      , TableColBuild(form =  AsciiTableTformCharacter(3), name = Some("COL_3"))
    )
    val rowSeq = Array(
        "       1 A abc"
      , "       2 B def"
      , "       3 C ghi"
    )

    FitsFactory.asciiTable(
      fitsName
      , mainHduBitPix = 8
      , mainHduAxisSeq = Array()
      , colDefSeq = simpleColDefSeq
      , rowSeq = rowSeq
      , saveFile = true)

    val optFits = FitsLoad.load(fitsName)
    if (optFits.isEmpty) Disconformity.print(fitsName)
    assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")

    if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
    assert(true)
  }
  //---------------------------------------------------------------------------

    TEST_14 must "have a valid structure with: binary table" in {
      initialActions(NORMALIZED_PATH)
      val fitsName = NORMALIZED_PATH + "/bin_table_1.fits"

      val colDefSq = Array(
          TableColBuild(form = BinTableTformLogical(), name = Some("COL_L"))
        , TableColBuild(form = BinTableTformBit(11), name = Some("COL_X"))
        , TableColBuild(form = BinTableTformCharacter(7), name = Some("COL_A"))
      )

      val rowSeq =
          Array(
            //col 1                  //col 2                          //col 3
            Array(Array('T'.toByte), Array(0x3a.toByte,0x4B.toByte),  "vino  ".getBytes() :+ 0x00.toByte).flatten
          , Array(Array('F'.toByte), Array(0x00.toByte,0xFF.toByte),  "gerola".getBytes() :+ 0x00.toByte ).flatten
          )

      FitsFactory.binTable(
        fitsName
        , mainHduBitPix = 8
        , mainHduAxisSeq = Array()
        , colDefSeq = colDefSq
        , rowSeq = rowSeq
        , saveFile = true)

      val optFits = FitsLoad.load(fitsName)
      assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")

      if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
      assert(true)
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_3_1_spec.scala
//=============================================================================

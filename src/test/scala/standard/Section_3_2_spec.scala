/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/mar/2018
 * Time:  11h:41m
 * Description: Unit testing for FITS parserTform of keyword + values records
 * The parserTform implements the rules of the grammar defined in appendix A of FITS standard version 4.0 (2018 August 13)
 * Far a faster testing, please use a ramdisk with 10GiB and change the variable 'SPEC_DATA_OUT_DIR' to point to it
 */
//=============================================================================
package standard
//=============================================================================
import com.common.fits.standard.block.record.RecordCharacter.RecordString
import com.common.fits.standard.dataType.DataType._
import com.common.fits.standard.fits.FitsLoad
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.Extension.EXTENSION_CONFORMING_DUMP_NAME
import com.common.util.file.MyFile
import com.common.fits.standard.fits.factory.FitsFactory
import standard.CommonSpec._
//=============================================================================
import common.UnitSpec
//=============================================================================
object Section_3_2_spec {
  //---------------------------------------------------------------------------
  private val SECTION_NAME = SectionName.SECTION_3_2
  private val NORMALIZED_PATH = SPEC_DATA_OUT_DIR + normalizeSectionName(SECTION_NAME) + "_"
  //---------------------------------------------------------------------------
  //List of test case
  val TEST_0O = SECTION_NAME + "-Test 00"
  val TEST_01 = SECTION_NAME + "-Test 01"
  //---------------------------------------------------------------------------
  // Previous defined test case applicable to the verification of this section:
  val RELATED_TEST_CASE_LIST = List(
      Section_3_1_spec.TEST_0O
    , Section_3_1_spec.TEST_01
    , Section_3_1_spec.TEST_02
    , Section_3_1_spec.TEST_03
    , Section_3_1_spec.TEST_04
    , Section_3_1_spec.TEST_05
    , Section_3_1_spec.TEST_08
    , Section_3_1_spec.TEST_09
    , Section_3_1_spec.TEST_10)
  //---------------------------------------------------------------------------
  // Verification by code inpection applicable to the verification of this section:
  val VERIFICATION_BY_CODE_INSPECTION = List(
      "Block.isAllASCII"
    , "Block.readHeader"
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
import Section_3_2_spec._
class Section_3_2_spec extends UnitSpec {
  //---------------------------------------------------------------------------
  TEST_0O must "have not have ASCII-text in the primary header" in {
    //---------------------------------------------------------------------------
    initialActions()
    val fitsName = NORMALIZED_PATH + "0.fits"
    val badRecord = RecordString(keyword = "BAD_CHAR",value =  new String(Array(0x1F.toByte)))

    FitsFactory.primary(
      fitsName
      , bitPix = DATA_TYPE_INT_16.bitSize
      , axisSeq = Array(0, 0)
      , recordSeq = Array(badRecord))

    val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
    assert(optFits.isEmpty, s"The FITS file '$fitsName' must be not be created")
    if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
    assert(true)
  }
  //---------------------------------------------------------------------------
  TEST_01 must "have not have ASCII-text in the extension header" in {
    initialActions()
    val fitsName = NORMALIZED_PATH + "0.fits"
    val badRecord = RecordString(keyword = "BAD_CHAR",value =  new String(Array(0x7F.toByte)))

    FitsFactory.extension(
      fitsName
      , mainHduBitPix = DATA_TYPE_INT_32.bitSize
      , mainHduAxisSeq = Array(8, 80)
      , extName = EXTENSION_CONFORMING_DUMP_NAME
      , extBitPix = 8
      , extensionAxisSeq = Array(1024)
      , extRecordSeq = Array(badRecord)
      , pcount = 0
      , gcount = 1)

    val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
    assert(optFits.isEmpty, s"The FITS file '$fitsName' must be not be created")
    if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
    assert(true)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_3_2_spec.scala
//=============================================================================

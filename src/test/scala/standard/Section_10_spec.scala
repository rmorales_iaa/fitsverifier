/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  28/mar/2018
  * Time:  11h:41m
  * Description: Unit testing for FITS section 8
  */
//=============================================================================
package standard
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.section_10.Section_10
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.fits.factory.FitsFactory
import com.common.logger.MyLogger
import common.UnitSpec
//=============================================================================
object Section_10_spec extends UnitSpec with MyLogger {
  //---------------------------------------------------------------------------
  private val recordMap = RecordMap()
  //---------------------------------------------------------------------------
  def buildMap(seq: Seq[(String,String)]) = {
    recordMap.clear()
    seq.foreach { case (keyword,value) =>
      val record = FitsFactory.buildRecord(keyword, value)
      if (record.isEmpty) error(s"Can not build record with keyword '$keyword' and value: '$value'")
      else {
        recordMap.append(record.get)
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import standard.Section_10_spec._
class Section_10_spec extends UnitSpec {

  //---------------------------------------------------------------------------
  //Section 10.1.1
  //---------------------------------------------------------------------------
  "ZIMAGE keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE,   "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX,  "-64")
      , (KEYWORD_ZNAXIS,   "1")
      , (KEYWORD_ZNAXIS1,  "1")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "'T'")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "1")
      , (KEYWORD_ZNAXIS1, "1")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZCMPTYPE keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_2'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "1")
      , (KEYWORD_ZNAXIS1, "1")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZBITPIX keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-65")
      , (KEYWORD_ZNAXIS, "1")
      , (KEYWORD_ZNAXIS1, "1")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZNAXIS keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //Section 10.1.2
  //---------------------------------------------------------------------------
  "ZTILE keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZTILE+1, "1323")
      , (KEYWORD_ZTILE+2, "723")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZNAME keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZNAME + 1, "'PARAM_1'")
      , (KEYWORD_ZNAME + 2, "'PARAM_2'")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }

  //---------------------------------------------------------------------------
  "ZVAL keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZVAL + 1, "'PARAM_1_VALUE'")
      , (KEYWORD_ZVAL + 2, "'PARAM_2_VALUE'")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZMASKCMP keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZMASKCMP, "'NOCOMPRESS'")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZQUANTIZ keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZQUANTIZ, "'HCOMPRESS 1'")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZDITHER0 keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZDITHER0, "1")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZSIMPLE keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZSIMPLE, "T")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZEXTEND keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZEXTEND, "F")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZBLOCKED keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZBLOCKED, "F")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZTENSION keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZTENSION, "'BINTABLE'")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZPCOUNT keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZPCOUNT, "1")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZGCOUNT keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZGCOUNT, "1")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZHECKSUM keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZHECKSUM, "'0000000000000000'")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ZDATASUM keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ZIMAGE, "T")
      , (KEYWORD_ZCMPTYPE, "'RICE_1'")
      , (KEYWORD_ZBITPIX, "-64")
      , (KEYWORD_ZNAXIS, "2")
      , (KEYWORD_ZNAXIS1, "4")
      , (KEYWORD_ZNAXIS2, "72")
      , (KEYWORD_ZDATASUM, "'0000000000000000'")
    ))
    assert(Section_10.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_10_spec.scala
//=============================================================================

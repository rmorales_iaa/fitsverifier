/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/mar/2018
 * Time:  11h:41m
 * Description: Unit testing for FITS section 8
 */
//=============================================================================
package standard
//=============================================================================
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.section_9.Section_9
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.fits.factory.FitsFactory
import com.common.logger.MyLogger
import common.UnitSpec
//=============================================================================
object Section_9_spec extends UnitSpec with MyLogger {
  //---------------------------------------------------------------------------
  private val recordMap = RecordMap()
  //---------------------------------------------------------------------------
  def buildMap(seq: Seq[(String,String)]) = {
    recordMap.clear()
    seq.foreach { case (keyword,value) =>
      val record = FitsFactory.buildRecord(keyword, value)
      if (record.isEmpty) error(s"Can not build record with keyword '$keyword' and value: '$value'")
      else {
        recordMap.append(record.get)
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import standard.Section_9_spec._
class Section_9_spec extends UnitSpec {
  //---------------------------------------------------------------------------
  //9.2.1 Time scale
  //---------------------------------------------------------------------------
  "TIMESYS keyword" must "be checked properly" in {
    //---------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TIMESYS, "'GMT'")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)

    //---------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TIMESYS, "'UT()'")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TIMESYS, "'UT(3)'")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TIMESYS, "'VINO'")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)

    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TIMESYS, "'ET'")
      , (KEYWORD_TIMESYS, "'TAI'")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)

    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TIMESYS, "'LOCAL'")
      , (KEYWORD_TIMESYS, "'TAI'")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //9.2.2. Time reference value
  //MJDREF same test as EPOCH
  //JDREF same test as EPOCH
  //DATEREF same test as DATE-OBS
  //MJDREFI same test as EXTVER
  //MJDREFF same test as EPOCH
  //JDREFI same test as EXTVER
  //JDREFF same test as EPOCH

  //---------------------------------------------------------------------------
  //9.2.3 Time reference position
  //TREFPOS same test as EPOCH
  //---------------------------------------------------------------------------
  "TREFPOS keyword" must "be checked properly" in {

    //---------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TREFPOS, "'TOPOCENTER'")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)
    //---------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TREFPOS, "'TOPOCENTER'")
      , (KEYWORD_OBSGEO_B, "40.7128")
      , (KEYWORD_OBSGEO_L, "-74.0060")
      , (KEYWORD_OBSGEO_H, "10.0")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)
    //---------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TREFPOS, "'CUSTOM'")
      , (KEYWORD_OBSGEO_B, "40.7128")
      , (KEYWORD_OBSGEO_L, "-74.0060")
      , (KEYWORD_OBSGEO_H, "10.0")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TREFPOS, "'GEOCENTER'")
      , (KEYWORD_TIMESYS, "'TT'")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TREFPOS, "'BARYCENTER'")
      , (KEYWORD_TIMESYS, "'TCB'")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------

    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (KEYWORD_TREFPOS, "'HELIOCENTER'")
      , (KEYWORD_TIMESYS, "'TAI'")
    ))
    assert(Section_9.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //TRPOS same test as TREFPOS
  //OBSGEO_B same test as EPOCH
  //OBSGEO_L same test as EPOCH
  //OBSGEO_H same test as EPOCH
  //OBSORBIT same test as EPOCH
  //---------------------------------------------------------------------------
  //9.2.4 Time reference direction
  //-------------------------------------------------------------------------
  buildMap(Seq(
    (KEYWORD_NAXIS, "1")
    , (KEYWORD_TREFDIR, "'BARYCENTER'")
  ))
  assert(Section_9.verify(recordMap, FitsCtxt()) == false)

  //-------------------------------------------------------------------------
  buildMap(Seq(
      (KEYWORD_NAXIS, "1")
    , (KEYWORD_TIMESYS, "'TCB'")
    , (KEYWORD_TREFPOS, "'BARYCENTER'")
    , (KEYWORD_TREFDIR, "'TREFPOS,TIMESYS'")
  ))
  assert(Section_9.verify(recordMap, FitsCtxt()) == true)
  //---------------------------------------------------------------------------
  //9.2.5 Solar system ephemeris
  //PLEPHEM same test as EPOCH

  //---------------------------------------------------------------------------
  //9.3 Time unit
  //TIMEUNIT same test as EPOCH

  //---------------------------------------------------------------------------
  //9.4.1 Time offset
  //TIMEOFFS same test as EPOCH

  //---------------------------------------------------------------------------
  //9.4.2 Time resolution and binding
  //TIMEDEL same test as EPOCH
  //TIMEPIXR same test as EPOCH

  //---------------------------------------------------------------------------
  //9.4.3 Time errors
  //TIMESYER same test as EPOCH
  //TIMRDER same test as EPOCH

  //---------------------------------------------------------------------------
  //9.5 Global time keywords
  //DATE-BEG same test as DATE-OBS
  //DATE-END same test as DATE-OBS
  //MJD-BEG same test as EPOCH
  //MJD-END same test as EPOCH
  //TSTART same test as EPOCH
  //TSTOP same test as EPOCH
  //JEPOCH same test as EPOCH
  //BEPOCH same test as EPOCH
  //---------------------------------------------------------------------------
  //9.6 Other time-coordinate axes
  //CZPHS same test as CDELT
  //CPERI same test as CDELT
  //---------------------------------------------------------------------------
  //9.7 Durations
  //XPOSURE same test as EPOCH
  //TELAPSE same test as EPOCH
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_9_spec.scala
//=============================================================================

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/mar/2018
 * Time:  11h:41m
 * Description: Unit testing for FITS parserTform of keyword + values records
 * The parser AsciiTableTform implements the rules of the grammar defined in appendix A of FITS standard version 4.0 (2018 August 13)
 * Far a faster testing, please use a ramdisk with 10GiB and change the variable 'SPEC_DATA_OUT_DIR' to point to it
 */
//=============================================================================
package standard
//=============================================================================
import com.common.fits.standard.dataType.DataType._
import com.common.fits.standard.fits.FitsLoad
import com.common.fits.standard.sectionName.SectionName
import com.common.util.file.MyFile
import com.common.fits.standard.fits.factory.FitsFactory
import standard.CommonSpec._
//=============================================================================
import common.UnitSpec
//=============================================================================
object Section_3_3_spec {
  //---------------------------------------------------------------------------
  private val SECTION_NAME = SectionName.SECTION_3_2
  private val NORMALIZED_PATH = SPEC_DATA_OUT_DIR + normalizeSectionName(SECTION_NAME) + "_"
  //---------------------------------------------------------------------------
  //List of test case
  val TEST_0O = SECTION_NAME + "-Test 00"
  val TEST_01 = SECTION_NAME + "-Test 01"
  val TEST_02 = SECTION_NAME + "-Test 02"
  //---------------------------------------------------------------------------
  // Previous defined test case applicable to the verification of this section:
  val RELATED_TEST_CASE_LIST = List(
    Section_3_1_spec.TEST_0O
    , Section_3_1_spec.TEST_05
    , Section_3_1_spec.TEST_06
    , Section_3_1_spec.TEST_07)
  //---------------------------------------------------------------------------
  // Verification by code inpection applicable to the verification of this section:
  val VERIFICATION_BY_CODE_INSPECTION = List(
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
import standard.Section_3_3_spec._
class Section_3_3_spec extends UnitSpec {
  //---------------------------------------------------------------------------
  TEST_0O must "have a valid structure sort with one primary with zero data and NAXIS = 0" in {
    initialActions()
    val fitsName = NORMALIZED_PATH + "0.fits"

    FitsFactory.primary(
      fitsName
      , bitPix = DATA_TYPE_INT_16.bitSize
      , axisSeq = Array())

    val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
    assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")
    if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
    assert(true)
  }
  //---------------------------------------------------------------------------
  TEST_01 must "have a valid structure sort with one primary with zero data NAXIS1 = 0" in {
    initialActions()
    val fitsName = NORMALIZED_PATH + "0.fits"

    FitsFactory.primary(
      fitsName
      , bitPix = DATA_TYPE_INT_16.bitSize
      , axisSeq = Array(0, 1))

    val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
    assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")
    if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
    assert(true)
  }
  //---------------------------------------------------------------------------
  TEST_02 must "have a valid structure sort with one primary with zero data NAXIS2 = 0" in {
    initialActions()
    val fitsName = NORMALIZED_PATH + "0.fits"

    FitsFactory.primary(
      fitsName
      , bitPix = DATA_TYPE_INT_16.bitSize
      , axisSeq = Array(1, 0))

    val optFits = FitsLoad.load(fitsName,readDataBlockFlag = false)
    assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")
    if (DELETE_GENERATED_FITS_FILES) MyFile.deleteFile(fitsName)
    assert(true)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file Section_3_3_spec.scala
//=============================================================================

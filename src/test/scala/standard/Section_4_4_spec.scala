/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/mar/2018
 * Time:  11h:41m
 * Description: Unit testing for FITS parserTform of keyword + values records
 * The parserTform implements the rules of the grammar defined in appendix A of FITS standard version 4.0 (2018 August 13)
 */
//=============================================================================
package standard
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.section_4_4.Section_4_4
import com.common.fits.standard.fits.{FitsCtxt}
import com.common.fits.standard.fits.factory.FitsFactory
import com.common.logger.MyLogger
import common.UnitSpec
//=============================================================================
object Section_4_4_spec extends UnitSpec with MyLogger {
  //---------------------------------------------------------------------------
  val recordMap = RecordMap()
  //---------------------------------------------------------------------------
  def buildMap(seq: Seq[(String,String)]) = {
    recordMap.clear()
    seq.foreach { case (keyword,value) =>
      val record = FitsFactory.buildRecord(keyword, value)
      if (record.isEmpty) error(s"Can not build record with keyword '$keyword' and value: '$value'")
      else {
        recordMap.append(record.get)
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import standard.Section_4_4_spec._
class Section_4_4_spec extends UnitSpec {
  //---------------------------------------------------------------------------
  //section 4.4.1.1
  //test by code inspection in class 'HDU' methods :
  //  isValidSimpleRecord
  //  isValidRecordBitPix
  //  isValidRecordNaxis
  //  isValidRecordAxisN
  //  isValidRecordEnd
  //---------------------------------------------------------------------------
  //section 4.4.1.2
  //test by code inspection in class 'Extension' methods :
  // isValidPcountRecord
  // isValidGcountRecord
  //---------------------------------------------------------------------------
  "DATE keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_DATE, "'0000-01-01T00:00:00'")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap( Seq(
      (KEYWORD_DATE, "'9999-12-31T23:59:59'")
     ))
    assert(Section_4_4.verify(recordMap,FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_DATE, "'-99999-01-01T00:00:00'")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_DATE, "'+99999-12-31T23:59:59'")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_DATE, "'-04713-11-24T12:00:00'")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_DATE, "'-04713-11-24T12:00:00.2342469545'")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_DATE, "'+99998-12-31T23:59:59.2342778912345567682'")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "ORIGIN keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ORIGIN, "'a'")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)

    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_ORIGIN, "")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "EXTEND keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_EXTEND, "F")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "BLOCKED keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_BLOCKED, "T")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  //section 4.4.2.2
  //  DATE-OBS same test as DATE
  //  DATEXXX  see tests on section 9.5
  //  TELESCOP same test as ORIGIN
  //  INSTRUME same test as ORIGIN
  //  OBSERVER same test as ORIGIN
  //  OBJECT same test as ORIGIN
  //---------------------------------------------------------------------------
  //section 4.4.2.3
  //  AUTHOR by ORIGIN
  //  REFERENC by ORIGIN
  //---------------------------------------------------------------------------
  //section 4.4.2.4
  //  COMMENT by inspection code in class 'token'
  //  HISTORY by inspection code in class 'token'
  //  'Keyword field is blank' by inspection code test in class 'token'
  //---------------------------------------------------------------------------
  //section 4.4.2.5
  "BSCALE keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_BSCALE, "T")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == false)

    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_BSCALE, "'3'")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == false)

    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_BSCALE, "3")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_BSCALE, "3.0")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "BLANK keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_BITPIX, "16")
       , (KEYWORD_BSCALE, "1")
       , (KEYWORD_BZERO, "0")
       , (KEYWORD_BLANK, "34")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_BITPIX, "16")
      , (KEYWORD_BSCALE, "1")
      , (KEYWORD_BZERO, "3")
      , (KEYWORD_BLANK, "34")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_BITPIX, "16")
      , (KEYWORD_BSCALE, "1")
      , (KEYWORD_BZERO, "121")
      , (KEYWORD_BLANK, "34")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //  BZERO same test as BSCALE
  //  BUNIT tests defined in class 'PhysicalUnitSpec'
  //  DATAMAX same test as BSCALE
  //  DATAMIN same test as BSCALE
  //  'WCS keywords' tests defined in class 'Section_8_spec'

  //---------------------------------------------------------------------------
  //section 4.4.2.6
  //---------------------------------------------------------------------------
  buildMap(Seq(
    (KEYWORD_EXTVER, "3.0")
  ))
  assert(Section_4_4.verify(recordMap, FitsCtxt()) == false)
  //---------------------------------------------------------------------------
  buildMap(Seq(
    (KEYWORD_EXTVER, "T")
  ))
  assert(Section_4_4.verify(recordMap, FitsCtxt()) == false)
  //---------------------------------------------------------------------------
  buildMap(Seq(
    (KEYWORD_EXTVER, "'3'")
  ))
  assert(Section_4_4.verify(recordMap, FitsCtxt()) == false)
  //---------------------------------------------------------------------------
  buildMap(Seq(
    (KEYWORD_EXTVER, "3")
  ))
  assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
  //---------------------------------------------------------------------------
  //  EXTNAME same test as ORIGIN
  //  EXTLEVEL same test as EXTVER
  //  INHERIT test by code inspection in method 'Extension.isValidInheritRecord'
  //---------------------------------------------------------------------------
  //section 4.4.2.7
  //---------------------------------------------------------------------------
  "DATASUM keyword" must "be checked properly" in {

    //---------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_DATASUM, "'0'")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)

    //---------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_DATASUM, "'00000000'")
    ))
    assert(Section_4_4.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //===========================================================================
}
//=============================================================================
//=============================================================================
//End of file Section_4_4_spec.scala
//=============================================================================

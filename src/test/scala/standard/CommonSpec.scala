/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Oct/2022
 * Time:  08h:48m
 * Description: None
 */
//=============================================================================
package standard
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import com.common.util.path.Path
//=============================================================================
//=============================================================================
object CommonSpec extends MyLogger {
  //---------------------------------------------------------------------------
  final val SPEC_DATA_IN_DIR = "/home/rafa/proyecto/scalaFitsVerifier/src/test/data/"
  Path.ensureDirectoryExist("/home/rafa/Downloads/deleteme/fits_files")
  final val SPEC_DATA_OUT_DIR = Path.ensureEndWithFileSeparator("/home/rafa/Downloads/deleteme/fits_files")
  //---------------------------------------------------------------------------
  final val DELETE_GENERATED_FITS_FILES = true
  //---------------------------------------------------------------------------
  def initialActions(dir: String = SPEC_DATA_OUT_DIR): Boolean = {
    MyConf.c = MyConf()
    Path.ensureDirectoryExist(dir)
    if (!Path.directoryExist(dir)) error(s"Directory: '$dir' does not exist")
    else true
  }
  //---------------------------------------------------------------------------
  def normalizeSectionName(sectionName: String) =
    sectionName.trim
      .replaceAll(" +", " ")
      .replace(" ","_")
      .replace(".","_")
      .replaceAll("_+", "_")
      .toLowerCase
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CommonSpec.scala
//=============================================================================

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/mar/2018
 * Time:  11h:41m
 * Description: Unit testing for FITS section 8
 */
//=============================================================================
package standard
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.block.record.verifier.section_8.Section_8
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.fits.factory.FitsFactory
import com.common.logger.MyLogger
import common.UnitSpec
//=============================================================================
object Section_8_spec extends UnitSpec with MyLogger {
  //---------------------------------------------------------------------------
  private val recordMap = RecordMap()
  //---------------------------------------------------------------------------
  def buildMap(seq: Seq[(String,String)]) = {
    recordMap.clear()
    seq.foreach { case (keyword,value) =>
      val record = FitsFactory.buildRecord(keyword, value)
      if (record.isEmpty) error(s"Can not build record with keyword '$keyword' and value: '$value'")
      else {
        recordMap.append(record.get)
      }
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import standard.Section_8_spec._
class Section_8_spec extends UnitSpec {

  //---------------------------------------------------------------------------
  "WCSAXES keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap( Seq(
        (KEYWORD_NAXIS,"1")
      , (KEYWORD_WCSAXES,"-1")
     ))
    assert(Section_8.verify(recordMap,FitsCtxt()) == false)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
        (KEYWORD_WCSAXES,  "1")
      , (KEYWORD_NAXIS, "1")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
    assert(true)
  }
  //---------------------------------------------------------------------------
  "PC keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
        (KEYWORD_NAXIS, "2")
      , (s"${KEYWORD_PC}1_1", "0")
      , (s"${KEYWORD_PC}1_2", "0")
      , (s"${KEYWORD_PC}2_1", "0")
      , (s"${KEYWORD_PC}2_2", "0")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
    buildMap(Seq(
        (KEYWORD_NAXIS, "2")
      , (s"${KEYWORD_PC}1_1", "1")
      , (s"${KEYWORD_PC}1_2", "1")
      , (s"${KEYWORD_PC}2_1", "1")
      , (s"${KEYWORD_PC}2_2", "2")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "CD keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
        (KEYWORD_NAXIS, "2")
      , (s"${KEYWORD_CD}1_1", "0")
      , (s"${KEYWORD_CD}1_2", "0")
      , (s"${KEYWORD_CD}2_1", "0")
      , (s"${KEYWORD_CD}2_2", "0")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
    buildMap(Seq(
        (KEYWORD_NAXIS, "2")
      , (s"${KEYWORD_CD}1_1", "1")
      , (s"${KEYWORD_CD}1_2", "1")
      , (s"${KEYWORD_CD}2_1", "1")
      , (s"${KEYWORD_CD}2_2", "2")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "CDELT keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
        (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CDELT}1", "-1")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    buildMap(Seq(
        (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CDELT}1", "0")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "CTYPE keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
        (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CTYPE}1", "")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CTYPE}1", "'aaa'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CTYPE}1", "'RA---UV '")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CTYPE}1", "'RA---HPX'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
   //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CTYPE}1", "'RA---TAN-SIP'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)

    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CTYPE}1", "'DEC--TAN-SIP'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------

    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CTYPE}1", "'AWAV-A2W'")  //spectral type
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "CUNIT keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CUNIT}1", "'deg'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CUNIT}1", "'kg'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "CRPIX keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"${KEYWORD_CRPIX}1", "3.234")
      , (s"${KEYWORD_CRPIX}2", "-3.234")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
  }
  //---------------------------------------------------------------------------
  "CRVAL keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"${KEYWORD_CRVAL}1", "3.234")
      , (s"${KEYWORD_CRVAL}1", "-3.234")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"${KEYWORD_CRVAL}1", "3.234")
      , (s"${KEYWORD_CRVAL}2", "-3.234")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "PV keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_PV}1_1", "-1")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_PV}1_1", "3.556")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "PS keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_PS}1_1", "'a'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_PS}1_1", "'b'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }

  //---------------------------------------------------------------------------
  "CRDER keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CRDER}1", "2")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CRDER}1", "0")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "CSYER keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CSYER}1", "-2")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "1")
      , (s"${KEYWORD_CSYER}1", "0")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "wcs alternative definition" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    val suffix = "A"
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_WCSAXES$suffix", "2")
      , (s"${KEYWORD_CTYPE}1$suffix", "'RA---TAN-SIP'")
      , (s"${KEYWORD_CTYPE}2$suffix", "'DEC--TAN-SIP'")
      , (s"${KEYWORD_CUNIT}1$suffix", "'deg'")
      , (s"${KEYWORD_CUNIT}2$suffix", "'kg'")
      , (s"${KEYWORD_CRPIX}1$suffix", "3.234")
      , (s"${KEYWORD_CRPIX}2$suffix", "-3.234")
      , (s"${KEYWORD_CRVAL}1$suffix", "23.234")
      , (s"${KEYWORD_CRVAL}1$suffix", "-33.234")
      , (s"${KEYWORD_CD}1_1$suffix", "1")
      , (s"${KEYWORD_CD}1_2$suffix", "1")
      , (s"${KEYWORD_CD}2_1$suffix", "1")
      , (s"${KEYWORD_CD}2_2$suffix", "2")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_WCSAXES", "2")
      , (s"${KEYWORD_CTYPE}1", "'RA---TAN-SIP'")
      , (s"${KEYWORD_CTYPE}2", "'DEC--TAN-SIP'")
      , (s"${KEYWORD_CUNIT}1", "'deg'")
      , (s"${KEYWORD_CUNIT}2", "'deg'")
      , (s"${KEYWORD_CRPIX}1", "3.234")
      , (s"${KEYWORD_CRPIX}2", "-3.234")
      , (s"${KEYWORD_CRVAL}1", "23.234")
      , (s"${KEYWORD_CRVAL}2", "-33.234")
      , (s"${KEYWORD_CD}1_1", "1")
      , (s"${KEYWORD_CD}1_2", "1")
      , (s"${KEYWORD_CD}2_1", "1")
      , (s"${KEYWORD_CD}2_2", "2")

      , (s"$KEYWORD_WCSAXES$suffix", "1")
      , (s"${KEYWORD_CTYPE}1$suffix", "'RA---TAN-SIP'")
      , (s"${KEYWORD_CUNIT}1$suffix", "'kg'")
      , (s"${KEYWORD_CRPIX}1$suffix", "-1133.234")
      , (s"${KEYWORD_CRVAL}1$suffix", "-31233.234")
      , (s"${KEYWORD_CD}1_1$suffix", "1123")
      ,
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
  }
  //---------------------------------------------------------------------------
  "RADESYS keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_RADESYS", "'GUEROLA'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_RADESYS", "'FK4'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_RADESYS", "'FK5'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_RADESYS", "'ICRS'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_EQUINOX", "1984.0")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "EQUINOX keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_EQUINOX" , "1984.0")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)

    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_EQUINOX", "-1")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  "EPOCH keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_EPOCH", "-2")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      ,(s"$KEYWORD_EPOCH", "'-2'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_EPOCH", "T")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //DATE-OBS see test in class "Section 4_4"
  //MJD-OBS same test as EPOCH
  //LONPOLE same test as EPOCH
  //LATPOLE same test as EPOCH
  //---------------------------------------------------------------------------
  //CNAME same test as PS
  //RESTFRQ same test as EPOCH. There is no way to check unit. It indicates that must be 'Hz' but this keyword has a WCS suffix not an axis index as is required in CUNIT
  //RESTWAV same test as EPOCH. There is no way to check unit. It indicates that must be 'm' but this keyword has a WCS suffix not an axis index as is required in CUNIT
  //---------------------------------------------------------------------------
  //DATE_AVG same test as DATE - OBS
  //MJD_AVG same test as EPOCH
  //---------------------------------------------------------------------------
  "SPECSYS keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_SPECSYS", "'TOPOCENT'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_SPECSYS", "'TOPOCENt'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //SYSOBS same test as SPECSYS
  //OBSGEO_X same test as EPOCH. To be implemented: "The coordinate must be valid at the epoch MJD-AVG or DATE-AVG"
  //OBSGEO_Y same test as EPOCH. To be implemented: "The coordinate must be valid at the epoch MJD-AVG or DATE-AVG"
  //OBSGEO_Z same test as EPOCH. To be implemented: "The coordinate must be valid at the epoch MJD-AVG or DATE-AVG"
  //---------------------------------------------------------------------------
  "SSYSSRC keyword" must "be checked properly" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_SSYSSRC", "'TOPOCENT'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == true)
    //-------------------------------------------------------------------------
    buildMap(Seq(
      (KEYWORD_NAXIS, "2")
      , (s"$KEYWORD_SSYSSRC", "'SOURCE'")
    ))
    assert(Section_8.verify(recordMap, FitsCtxt()) == false)
    //-------------------------------------------------------------------------
  }
  //VELOSYS same test as EPOCH.
  //ZSOURCE same test as EPOCH.
  //VELANGL same test as EPOCH.
}
//=============================================================================
//=============================================================================
//End of file Section_8_spec.scala
//=============================================================================

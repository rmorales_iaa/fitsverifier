/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  22/Nov/2022
  * Time:  09h:27m
  * Description: None
  */
//=============================================================================
package standard.table.binTable

//=============================================================================
import com.common.fits.standard.block.record.RecordCharacter.RecordString
import com.common.fits.standard.block.record.RecordNumber.RecordInteger
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.dataType.Conversion._
import com.common.fits.standard.fits.{FitsCtxt, FitsLoad}
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.{BinTable, BinTableColDef, BinTableRow}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.CommonTable.{isValidTscal, isValidTzero}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.{CommonTable, TableColDef}
import common.UnitSpec
//=============================================================================
//=============================================================================
object BinTable_Spec {
  //-------------------------------------------------------------------------
  def processRowSeq(rowValueSeq: Array[(Array[Byte],Boolean,Array[String])]
                   , colDefSeq: Array[BinTableColDef]): Boolean = {
    //process all row values
    rowValueSeq.zipWithIndex.foreach { case ((row, expectedResult, expectedValueSeq), processedRowCount) =>

      val (result, calculatedValueSeq) = BinTableRow.getRowValueSeq(
        row
        , colDefSeq
        , processedRowCount + 1
        , applyLinearTransformation = true)

      if (result && expectedResult == false) return false
      if (!result && expectedResult == true) return false

      if (result) {
        expectedValueSeq.zipWithIndex.foreach { case (expectedValue, i) =>
          if (calculatedValueSeq(i) != expectedValue)
            return false
        }
      }
    }
    true
  }
  //-------------------------------------------------------------------------
}

//=============================================================================
import BinTable_Spec._
class BinTable_Spec  extends UnitSpec {
  //---------------------------------------------------------------------------
  "Test 1-0: an binary table well defined with a single row and two columns: logical and logical array" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex = 1L //index of the column, starting with index 1
    var byteOffset = 1L //offset of the column in characters
    //-------------------------------------------------------------------------
    //Logical
    var fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "1L"
      , name = Some("logical")
      , unit = Some("km.s")
      , scale = None
      , zero = None
      , nullValue = None
      , disp = Some("L8")
      , tdMin = None
      , tdMax = None
      , tlMin = None
      , tlMax = None
      , tDim = None
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "12L"
      , name = Some("logical_array")
      , unit = Some("km.s")
      , scale = None
      , zero = None
      , nullValue = None
      , disp = Some("L2")
      , tdMin = None
      , tdMax = None
      , tlMin = None
      , tlMax = None
      , tDim = Some("(3,2)")
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values
    val rowValueSeq = Array(
      (Array("F".getBytes(), "TFTFFTFTTFFT".getBytes()).flatten
        , true
        , Array("       F"
               ,"{{{ T,  F,  T}, { F,  F,  T}},{{ F,  T,  T}, { F,  F,  T}}}"))

    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(recordMap, isAciiTable = false, SectionName.SECTION_7_3_2).get)
    //-------------------------------------------------------------------------
    assert(processRowSeq(rowValueSeq, colDefSeq))
  }

  //---------------------------------------------------------------------------
  "Test 1-1: an binary table well defined with a single row and two columns: bit and bit array" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex = 1L //index of the column, starting with index 1
    var byteOffset = 1L //offset of the column in characters
    //-------------------------------------------------------------------------

    //bit array 1
    var fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "X"
      , name = Some("bit")
      , unit = Some("km.s")
      , scale = None
      , zero = None
      , nullValue = None
      , disp = Some("B2.1")
      , tdMin = None
      , tdMax = None
      , tlMin = None
      , tlMax = None
      , isAsciiTable = false
    )

    //bit array more than 1
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "3X"
      , name = Some("bit array")
      , unit = Some("km.s")
      , scale = None
      , zero = None
      , nullValue = None
      , disp = Some("B7.1")
      , tdMin = None
      , tdMax = None
      , tlMin = None
      , tlMax = None
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values
    val rowValueSeq = Array(
      (Array(0x10.toByte, 0x50.toByte)
        , true
        , Array("10", "1010000"))
    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(recordMap, isAciiTable = false, SectionName.SECTION_7_3_2).get)
    //-------------------------------------------------------------------------
    assert(processRowSeq(rowValueSeq, colDefSeq))
  }
  //---------------------------------------------------------------------------
  "Test 1-2: an binary table well defined with a single row and two columns: character and character array" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex = 1L //index of the column, starting with index 1
    var byteOffset = 1L //offset of the column in characters
    //-------------------------------------------------------------------------

    //Unsigned byte
    var fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "A"
      , name = Some("Character")
      , unit = Some("km.s")
      , scale = None
      , zero = None
      , nullValue = None
      , disp = Some("A3")
      , tdMin = None
      , tdMax = None
      , tlMin = None
      , tlMax = None
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "8A"
      , name = Some("Character array")
      , unit = Some("km.s")
      , scale = None
      , zero = None
      , nullValue = None
      , disp = Some("A2")
      , tdMin = None
      , tdMax = None
      , tlMin = None
      , tlMax = None
      , tDim = Some("(5,4,3)")
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values
    val rowValueSeq = Array(
      (Array('a'.toByte) ++ "bcdefghi".getBytes()
        , true
        , Array("  a", "bc"))
    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(recordMap, isAciiTable = false, SectionName.SECTION_7_3_2).get)
    //-------------------------------------------------------------------------
    assert(processRowSeq(rowValueSeq, colDefSeq))
  }
  //---------------------------------------------------------------------------
  "Test 1-3: an binary table well defined with a single row and two columns: unsigned byte and unsigned byte array" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex = 1L //index of the column, starting with index 1
    var byteOffset = 1L //offset of the column in characters
    //-------------------------------------------------------------------------

    //Unsigned byte
    var fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "B"
      , name = Some("unsigned byte")
      , unit = Some("km.s")
      , scale = None
      , zero = None
      , nullValue = Some("99")
      , disp = Some("Z8.3")
      , tdMin = Some(1)
      , tdMax = Some(99)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    //Unsigned byte array
    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "3B"
      , name = Some("unsigned byte array")
      , unit = Some("km.s")
      , scale = Some(1.3d)
      , zero = Some(1023)
      , nullValue = Some("99")
      , disp = Some("Z8.3")
      , tdMin = Some(1)
      , tdMax = Some(99999)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values
    val rowValueSeq = Array(
      (Array(0x35.toByte,0x13.toByte,0x99.toByte,0xFF.toByte)
        , true
        , Array("     035"
               ,"{{     040,     090,     05E,     0CC,     0CC,     0CC,     0CC,     0CD},{     040,     093,     017,     099,     099,     099,     099,     09A},{     040,     095,     02A,     000,     000,     000,     000,     000}}"))
    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(recordMap, isAciiTable = false, SectionName.SECTION_7_3_2).get)
    //-------------------------------------------------------------------------
    assert(processRowSeq(rowValueSeq, colDefSeq))
  }

  //---------------------------------------------------------------------------
  "Test 1-4: an binary table well defined with a single row and two columns: 16-bit integer and 16-bit integer array" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex = 1L //index of the column, starting with index 1
    var byteOffset = 1L //offset of the column in characters
    //-------------------------------------------------------------------------
    //16-bit integer
    var fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "I"
      , name = Some("16 bit integer")
      , unit = Some("km.s")
      , scale = None
      , zero = None
      , nullValue = Some("-999")
      , disp = Some("Z8.3")
      , tdMin = Some(-1025)
      , tdMax = Some(10243)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "3I"
      , name = Some("16 bit integer array")
      , unit = Some("km.s")
      , scale = Some(1.3d)
      , zero = Some(1023)
      , nullValue = Some("-999")
      , disp = Some("Z8.3")
      , tdMin = Some(-1025)
      , tdMax = Some(10243)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    //-------------------------------------------------------------------------
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values¡
    val rowValueSeq = Array(
      (Array(BigInt(-999).toByteArray, BigInt(-999).toByteArray, BigInt(999).toByteArray, BigInt(2394).toByteArray).flatten
        , true
        , Array("-999"
        , "{{     0C0,     071,     03B,     033,     033,     033,     033,     034},{     040,     0A2,     023,     066,     066,     066,     066,     066},{     040,     0B0,     027,     033,     033,     033,     033,     034}}"))
    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(recordMap, isAciiTable = false, SectionName.SECTION_7_3_2).get)
    //-------------------------------------------------------------------------
    assert(processRowSeq(rowValueSeq, colDefSeq))
  }

  //---------------------------------------------------------------------------
  "Test 1-5: an binary table well defined with a single row and two columns: 32-bit integer and 32-bit integer array" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex = 1L //index of the column, starting with index 1
    var byteOffset = 1L //offset of the column in characters
    //-------------------------------------------------------------------------
    //32-bit integer
    var fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "J"
      , name = Some("32 bit integer")
      , unit = Some("km.s")
      , scale = None
      , zero = None
      , nullValue = Some("-999")
      , disp = Some("Z8.3")
      , tdMin = Some(-9911025)
      , tdMax = Some(1110243)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "3J"
      , name = Some("16 bit integer array")
      , unit = Some("km.s")
      , scale = Some(1.3d)
      , zero = Some(1023)
      , nullValue = Some("-999")
      , disp = Some("I32.3")
      , tdMin = Some(-10e10)
      , tdMax = Some(10e10)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    //-------------------------------------------------------------------------
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values
    val rowValueSeq = Array(
      (Array(BigInt(-9213861).toByteArray, BigInt(-9213861).toByteArray, BigInt(9213861).toByteArray, BigInt(9233861).toByteArray).flatten
        , true
        , Array("FF73685B"
        , "{                       -11976996,                        11979042,                        12005042}"))
    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(recordMap, isAciiTable = false, SectionName.SECTION_7_3_2).get)
    //-------------------------------------------------------------------------
    assert(processRowSeq(rowValueSeq, colDefSeq))
  }


  //---------------------------------------------------------------------------
  "Test 1-6: an binary table well defined with a single row and two columns: 64-bit integer and 64-bit integer array" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex = 1L //index of the column, starting with index 1
    var byteOffset = 1L //offset of the column in characters
    //-------------------------------------------------------------------------
    //64-bit integer
    var fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "K"
      , name = Some("64 bit integer")
      , unit = Some("km.s")
      , scale = Some(1.3d)
      , zero = Some(1023)
      , nullValue = Some("-999")
      , disp = Some("Z16.3")
      , tdMin = Some(-10e100)
      , tdMax = Some(10e100)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "3K"
      , name = Some("64 bit integer array")
      , unit = Some("m.s")
      , scale = Some(1.3d)
      , zero = Some(1023)
      , nullValue = Some("-999")
      , disp = Some("Z20.3")
      , tdMin = Some(-10e100)
      , tdMax = Some(10e100)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values
    val rowValueSeq = Array(
      (Array(BigInt(-45732792138614529L).toByteArray,BigInt(-45732792138614529L).toByteArray,BigInt(45732792138614529L).toByteArray,BigInt(95732792138614529L).toByteArray).flatten
        , true
        , Array("{             0C3,             06A,             066,             0FB,             041,             0E0,             05E,             03D}"
              , "{{                 0C3,                 06A,                 066,                 0FB,                 041,                 0E0,                 05E,                 03D},{                 043,                 06A,                 066,                 0FB,                 041,                 0E0,                 05F,                 03D},{                 043,                 07B,                 0A2,                 050,                 01E,                 056,                 097,                 09F}}"))
    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(recordMap, isAciiTable = false, SectionName.SECTION_7_3_2).get)
    //-------------------------------------------------------------------------
    assert(processRowSeq(rowValueSeq, colDefSeq))
  }
  //---------------------------------------------------------------------------
  "Test 1-7: an binary table well defined with a single row and two columns: 32-bit float and 32-bit float array" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex = 1L //index of the column, starting with index 1
    var byteOffset = 1L //offset of the column in characters
    //-------------------------------------------------------------------------
    //Single precision floating point
    var fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "E"
      , name = Some("Single_precision_floating_point")
      , unit = Some("km.s")
      , scale = Some(1.3d)
      , zero = Some(1023)
      , nullValue = None
      , disp = Some("D20.3E4")
      , tdMin = Some(-10e10)
      , tdMax = Some(10e10)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "3E"
      , name = Some("Single_precision_floating_point_array")
      , unit = Some("km.s")
      , scale = Some(1.3d)
      , zero = Some(1023)
      , nullValue = None
      , disp = Some("D20.3E4")
      , tdMin = Some(-10e10)
      , tdMax = Some(10e10)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values
    val rowValueSeq = Array(
      (Array(floatToByteSeq(323.123e-3f), floatToByteSeq(323.123e-3f), floatToByteSeq(-323.123e-3f), floatToByteSeq(32423.123e+3f)).flatten
        , true
        , Array("         0.102D+0004"
        , "{         0.102D+0004,         0.102D+0004,         0.422D+0008}"))
    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(recordMap, isAciiTable = false, SectionName.SECTION_7_3_2).get)
    //-------------------------------------------------------------------------
    assert(processRowSeq(rowValueSeq, colDefSeq))
  }
  //---------------------------------------------------------------------------
  "Test 1-8: an binary table well defined with a single row and two columns: 64-bit float and 64-bit float array" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex = 1L //index of the column, starting with index 1
    var byteOffset = 1L //offset of the column in characters
    //-------------------------------------------------------------------------
    //Double precision floating point
    var fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "D"
      , name = Some("Double_precision_floating_point")
      , unit = Some("km.s")
      , scale = Some(1.3d)
      , zero = Some(1023)
      , nullValue = None
      , disp = Some("D20.3E4")
      , tdMin = Some(-10e10)
      , tdMax = Some(10e10)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "3D"
      , name = Some("Double_precision_floating_point_array")
      , unit = Some("km.s")
      , scale = Some(1.3d)
      , zero = Some(1023)
      , nullValue = None
      , disp = Some("D20.3E4")
      , tdMin = Some(-10e10)
      , tdMax = Some(10e10)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values
    val rowValueSeq = Array(
      (Array(doubleToByteSeq(323.123e-3f), doubleToByteSeq(323.123e-3f), doubleToByteSeq(-323.123e-3f), doubleToByteSeq(32423.123e+3f)).flatten
        , true
        , Array("         0.102D+0004"
        , "{         0.102D+0004,         0.102D+0004,         0.422D+0008}"))
    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(recordMap, isAciiTable = false, SectionName.SECTION_7_3_2).get)
    //-------------------------------------------------------------------------
    assert(processRowSeq(rowValueSeq, colDefSeq))
  }
  //---------------------------------------------------------------------------
  "Test 1-9: an binary table well defined with a single row and two columns: 32-bit complex and 32-bit complex array" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex = 1L //index of the column, starting with index 1
    var byteOffset = 1L //offset of the column in characters
    //-------------------------------------------------------------------------
    //Single precision complex
    var fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "C"
      , name = Some("Single_precision_complex")
      , unit = Some("km.s")
      , scale = None
      , zero =  None
      , nullValue = None
      , disp = Some("F20.3")
      , tdMin = Some(-10e10)
      , tdMax = Some(10e10)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "3C"
      , name = Some("Single_precision_complex_array")
      , unit = Some("km.s")
      , scale = Some(1.3d)
      , zero = Some(1023)
      , nullValue = None
      , disp = Some("F20.3")
      , tdMin = Some(-10e10)
      , tdMax = Some(10e10)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values
    val rowValueSeq = Array(
      (Array(floatToByteSeq(323.123e-3f), floatToByteSeq(-323.123e+3f)
           , floatToByteSeq(323.123e-3f), floatToByteSeq(-2323.123e+3f)
           , floatToByteSeq(2323.123e-6f), floatToByteSeq(-3223.123e+3f)
           , floatToByteSeq(1323.123e+3f), floatToByteSeq(1323.123e+3f)).flatten
        , true
        , Array("(               0.323,         -323123.000)"
             , "{(               1.720,        -3020059.900),(               1.303,        -4190059.900),(         1720061.200,         1720059.900)}"))
    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(recordMap, isAciiTable = false, SectionName.SECTION_7_3_2).get)
    //-------------------------------------------------------------------------
    assert(processRowSeq(rowValueSeq, colDefSeq))
  }
  //---------------------------------------------------------------------------
  "Test 1-10: an binary table well defined with a single row and two columns: 64-bit complex and 64-bit complex array" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex = 1L //index of the column, starting with index 1
    var byteOffset = 1L //offset of the column in characters
    //-------------------------------------------------------------------------
    //Double precision complex
    var fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "M"
      , name = Some("Double_precision_complex")
      , unit = Some("km.s")
      , scale = None
      , zero = None
      , nullValue = None
      , disp = Some("F20.3")
      , tdMin = Some(-10e10)
      , tdMax = Some(10e10)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , byteOffset
      , form = "3M"
      , name = Some("Double_precision_complex_array")
      , unit = Some("km.s")
      , scale = Some(1.3d)
      , zero = Some(1023)
      , nullValue = None
      , disp = Some("F20.3")
      , tdMin = Some(-10e10)
      , tdMax = Some(10e10)
      , tlMin = Some(4096)
      , tlMax = Some(10100)
      , isAsciiTable = false
    )
    byteOffset += fieldSize + 1 //one more for the column divider
    colIndex += 1

    //-------------------------------------------------------------------------
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values
    val rowValueSeq = Array(
      (Array(doubleToByteSeq(323.123e-3f), doubleToByteSeq(-323.123e+3f)
           , doubleToByteSeq(323.123e-3f), doubleToByteSeq(-2323.123e+3f)
           , doubleToByteSeq(2323.123e-6f), doubleToByteSeq(-3223.123e+3f)
           , doubleToByteSeq(1323.123e+3f), doubleToByteSeq(1323.123e+3f)).flatten
        , true
        , Array("(               0.323,         -323123.000)"
              , "{(               1.720,        -3020059.900),(               1.303,        -4190059.900),(         1720061.200,         1720059.900)}"))
    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toBinTableColDef(
      TableColDef.buildColDefSeq(recordMap, isAciiTable = false, SectionName.SECTION_7_3_2).get)
    //-------------------------------------------------------------------------
    assert(processRowSeq(rowValueSeq, colDefSeq))
  }
  //---------------------------------------------------------------------------
  //fields from 'XTENSION' to 'NAXIS' super seed by Extension.isValidHeader
  "Test 2 an Binary table with a wrong NAXIS_1 definition" must "be property managed" in {

    val recordMap = RecordMap()

    recordMap.append(RecordInteger(KEYWORD_NAXIS, 2))
    recordMap.append(RecordInteger(KEYWORD_NAXIS + 1, 2)) //size of each row in bytes
    recordMap.append(RecordInteger(KEYWORD_NAXIS + 2, 3)) //row count

    recordMap.append(RecordString(KEYWORD_TFORM + 1, "I1"))
    recordMap.append(RecordString(KEYWORD_TFORM + 2, "A1"))
    recordMap.append(RecordString(KEYWORD_TFORM + 3, "4K"))

    assert(BinTable.isValidNaxis_1(recordMap) == false)  //expected size is 2 (KEYWORD_NAXIS1), but stored is 35 (due to TFORM definition)
  }

  //---------------------------------------------------------------------------
  //fields from 'XTENSION' to 'NAXIS' super seed by Extension.isValidHeader
  "Test 3 an Binary table with a wrong NAXIS_2 definition" must "be property managed" in {

    assert(true)  //It is validated when all data blocks are processed: BinTableRow.buildRowSeq
  }
  //---------------------------------------------------------------------------
  "Test 4: an Binary table with a wrong TFIELDS definition" must "be property managed" in {

    val recordMap = RecordMap()

    recordMap.append(RecordInteger(KEYWORD_TFIELDS, -1))  //must be in range [0,999]
    assert(CommonTable.isValidTfields(recordMap) == false)
    recordMap.remove(KEYWORD_TFIELDS)

    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1000))   //must be in range [0,999]
    assert(CommonTable.isValidTfields(recordMap) == false)
    recordMap.remove(KEYWORD_TFIELDS)

    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 0))
    assert(CommonTable.isValidTfields(recordMap) == true)
    recordMap.remove(KEYWORD_TFIELDS)

    recordMap.append(RecordString(KEYWORD_TFIELDS, "0"))
    assert(CommonTable.isValidTfields(recordMap) == false)   //must be an integer
    recordMap.remove(KEYWORD_TFIELDS)

    assert(true)
  }

  //---------------------------------------------------------------------------
  "Test 4: an Binary table with a wrong TFORM definition" must "be property managed" in {
    //see tests in 'BinTableTformTdisp_Spec'
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 5: an Binary table with a wrong TTYPE definition" must "be property managed" in {
    //see 'Test_1' in this class
    //code inspection 'BinaryTable.isValidHeader'
    val recordMap = RecordMap()
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1))     //column per row
    recordMap.append(RecordString(KEYWORD_TTYPE + 1, "-"))
    CommonTable.checkTtypeCharacterRange(recordMap, SectionName.SECTION_7_3_2)

    //check duplicated
    recordMap.clear()
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 2))
    recordMap.append(RecordString(KEYWORD_TTYPE + 1, "a"))
    recordMap.append(RecordString(KEYWORD_TFORM + 1, "A1"))

    recordMap.append(RecordString(KEYWORD_TTYPE + 2, "A"))
    recordMap.append(RecordString(KEYWORD_TFORM + 2, "A1"))
    val r = TableColDef.buildColDefSeq(recordMap
                                       , isAciiTable = false
                                       , SectionName.SECTION_7_3_2)
    assert(r.isEmpty)
  }
  //---------------------------------------------------------------------------
  "Test 6: an Binary table with a wrong TUNIT definition" must "be property managed" in {
    //see 'Test_1' in class 'PhysicalUnitSpec'
    //code inspection 'BinaryTable.isValidHeader'
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 7: an Binary table with a wrong TSCAL definition" must "be property managed" in {
    //see 'Test_1' in this class
    //code inspection 'BinaryTable.isValidHeader'

    val recordMap = RecordMap()

    //TFORM A
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1)) //column per row
    recordMap.append(RecordString(KEYWORD_TFORM + 1, "A"))
    recordMap.append(RecordString(KEYWORD_TSCAL + 1, "1.3"))
    assert(isValidTscal(recordMap, colCount  = 1, isAsciiTable = false, ctxt = FitsCtxt()) == false)

    //TFORM L
    recordMap.clear()
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1)) //column per row
    recordMap.append(RecordString(KEYWORD_TFORM + 1, "L"))
    recordMap.append(RecordString(KEYWORD_TSCAL + 1, "1.3"))
    assert(isValidTscal(recordMap, colCount = 1, isAsciiTable = false, ctxt = FitsCtxt()) == false)

    //TFORM X
    recordMap.clear()
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1)) //column per row
    recordMap.append(RecordString(KEYWORD_TFORM + 1, "X"))
    recordMap.append(RecordString(KEYWORD_TSCAL + 1, "1.3"))
    assert(isValidTscal(recordMap, colCount = 1, isAsciiTable = false, ctxt = FitsCtxt()) == false)

  }
  //---------------------------------------------------------------------------
  "Test 8: an Binary table with a wrong TZERO definition" must "be property managed" in {
    //see 'Test_1' in this class
    //code inspection 'BinaryTable.isValidHeader'
    val recordMap = RecordMap()

    //TFORM A
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1)) //column per row
    recordMap.append(RecordString(KEYWORD_TFORM + 1, "A"))
    recordMap.append(RecordString(KEYWORD_TZERO + 1, "1.3"))
    assert(isValidTzero(recordMap, colCount = 1, isAsciiTable = false, ctxt = FitsCtxt()) == false)

    //TFORM L
    recordMap.clear()
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1)) //column per row
    recordMap.append(RecordString(KEYWORD_TFORM + 1, "L"))
    recordMap.append(RecordString(KEYWORD_TZERO + 1, "1.3"))
    assert(isValidTzero(recordMap, colCount = 1, isAsciiTable = false, ctxt = FitsCtxt()) == false)

    //TFORM X
    recordMap.clear()
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1)) //column per row
    recordMap.append(RecordString(KEYWORD_TFORM + 1, "X"))
    recordMap.append(RecordString(KEYWORD_TZERO + 1, "1.3"))
    assert(isValidTzero(recordMap, colCount = 1, isAsciiTable = false, ctxt = FitsCtxt()) == false)
   }

  //---------------------------------------------------------------------------
  "Test 9: an Binary table with a wrong TNULL definition" must "be property managed" in {
    //see 'Test_1' in this class
    //code inspection: BinTable.isValidTypeForTnull
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 10: an Binary table with a wrong TDISP definition" must "be property managed" in {
    //code inspection CommonTable.isValidTdisp
    //code inspection: BinTable.isValidTdisp
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 11: an Binary table with a wrong THEAP definition" must "be property managed" in {
    //code inspection: BinTable.isValidTheap
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 12: an Binary table with a wrong TDMIN definition" must "be property managed" in {
    //code inspection: BinTable.isValidTdim
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 13: an Binary table with a wrong TDMAX definition" must "be property managed" in {
    //code inspection: TableColDef.isValueInRange
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 14: an Binary table with a wrong TLMIN definition" must "be property managed" in {
    //code inspection: TableColDef.isValueInRange
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 15: an Binary table with a wrong TLMAX definition" must "be property managed" in {
    //code inspection: TableColDef.isValueInRange
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 16: an Binary table with proper array descriptor" must "be property managed" in {

    //Array descriptors are manages as nominal arrays. Code inspection: BinTableRow.parseColSeq
   val fitsName = "/home/rafa/proyecto/scalaFitsVerifier/src/test/data/table/bin_table_var_array_1.fits"
    val optFits = FitsLoad.load(fitsName)
    assert(optFits.isDefined, s"The FITS file '$fitsName' must be created")
    assert(true)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BinTable_Spec.scala
//=============================================================================

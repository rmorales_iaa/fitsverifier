/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Nov/2022
 * Time:  09h:27m
 * Description: None
 */
//=============================================================================
package standard.table.binTable

//=============================================================================
import com.common.fits.standard.dataType.DataType.{DATA_TYPE_FLOAT_32, DATA_TYPE_FLOAT_64, DATA_TYPE_U_INT_8}
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.BinTableColDef
import com.common.fits.standard.structure.hdu.extension.conforming.standard.binTable.tform._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp._
import com.common.util.util.Util
import common.UnitSpec
//=============================================================================
//=============================================================================
object BinTableTformTdisp_Spec {
  //---------------------------------------------------------------------------
  private def performTest(rawInput: Array[Byte]
                          , tForm: BinTableTform
                          , tDisp: Tdisp
                          , expectedTformOutput: String
                          , expectedTdispOutput: String
                          , debug: Boolean = true): Boolean = {

    val colDef = BinTableColDef(colIndex = 0, position = 0, form = tForm, disp = Some(tDisp))

    //ensure the size of the input. The input comes from a sclice of a table row, so
    // the slice has that has always the proper size
    val input = {
      val tFormByteSize = tForm.getByteSize.toInt
      val a = rawInput.take(tFormByteSize)
      val paddingByteCount = tForm.getByteSize - a.size
      val padding = if (paddingByteCount > 0) Array.fill[Byte](paddingByteCount.toInt)(0)
                    else Array[Byte]()
      padding ++ a
    }

    if (debug) {
      println(s"------------------------------------")
      println(s"input:    '${Util.toHexString(input)}' size(${input.size})")
    }

    //format string following tForm
    val tFormString = tForm.format(input, FitsCtxt())
    if (debug) {
      println(s"TFORM:    '$tFormString' size(${tFormString.size})")
      println(s"expected: '$expectedTformOutput' size(${expectedTformOutput.size})")
      println(s"is valid result: '${tFormString == expectedTformOutput}'")
      if (tFormString != expectedTformOutput)
        println("ERROR!")
      println(s"........................................................")
    }
    if (tFormString != expectedTformOutput) return false

    //format string following tDisp
     val r = colDef.format(input)
     val tDispString = if (r.size > 1) r.mkString(Tdisp.VALUE_SEQ_START_CHAR, Tdisp.VALUE_SEQ_DIVIDER, Tdisp.VALUE_SEQ_END_CHAR)
                       else r.mkString("")

    if (debug) {
      println(s"TDISP:    '$tDispString' size(${tDispString.size})")
      println(s"expected: '$expectedTdispOutput' size(${expectedTdispOutput.size})")
      println(s"is valid result: '${tDispString == expectedTdispOutput}'")
      if (tDispString != expectedTdispOutput)
        println("ERROR!")
      println(s"+++++++++++++++++++++++++++++++++++")
    }
    tDispString == expectedTdispOutput
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import BinTableTformTdisp_Spec._
class BinTableTformTdisp_Spec  extends UnitSpec {
  //---------------------------------------------------------------------------
  "Test 0: testing" must "be properly displayed" in {
    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte
                                 , 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_16(2)
        , TdispFloatExponential(12, 2, 4)
        , expectedTformOutput = "{9029,26623}"
        , expectedTdispOutput = "{  0.90E+0004,  0.27E+0005}"
      ) == true)

    assert(true)
  }

  //---------------------------------------------------------------------------
  "Test 1: TFORM character" must "be properly displayed" in {

    assert(
      performTest(rawInput = Array('V'.toByte)
        , BinTableTformCharacter()
        , TdispCharacter(1)
        , expectedTformOutput = "V"
        , expectedTdispOutput = "V"
      ) == true)

    assert(
      performTest(rawInput = Array('V'.toByte)
        , BinTableTformCharacter()
        , TdispCharacter(2)
        , expectedTformOutput = "V"
        , expectedTdispOutput = " V"
      ) == true)

    assert(
      performTest(rawInput = "VI".getBytes()
        , BinTableTformCharacter(2)
        , TdispCharacter(2)
        , expectedTformOutput = "VI"
        , expectedTdispOutput = "VI"
      ) == true)

    assert(
      performTest(rawInput = "'VI'".getBytes()
        , BinTableTformCharacter(4)
        , TdispCharacter(2)
        , expectedTformOutput = "'VI'"
        , expectedTdispOutput = "'V"
      ) == true)

    assert(
      performTest(rawInput = "123".getBytes()
        , BinTableTformCharacter(3)
        , TdispInteger(8,4)
        , expectedTformOutput = "123"
        , expectedTdispOutput = "{    0049,    0050,    0051}"
      ) == true)

    assert(
      performTest(rawInput = "123E34".getBytes()
        , BinTableTformCharacter(6)
        , TdispGeneralExponential(20,1,4)
        , expectedTformOutput = "123E34"
        , expectedTdispOutput = "12300000000000000000"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 2: TFORM logical" must "be properly displayed" in {
   assert(
      performTest(rawInput = "T".getBytes()
        , BinTableTformLogical()
        , TdispLogical(1)
        , expectedTformOutput = "T"
        , expectedTdispOutput = "T"
      ) == true)

    assert(
      performTest(rawInput = "FFTFT".getBytes()
        , BinTableTformLogical(5)
        , TdispLogical(5)
        , expectedTformOutput = "FFTFT"
        , expectedTdispOutput = "{    F,    F,    T,    F,    T}"
      ) == true)

    assert(
      performTest(rawInput = "FFTFT".getBytes()
        , BinTableTformLogical(5)
        , TdispLogical(2)
        , expectedTformOutput = "FFTFT"
        , expectedTdispOutput = "{ F, F, T, F, T}"
      ) == true)

    assert(
      performTest(rawInput = "FFTFT".getBytes()
        , BinTableTformLogical(5)
        , TdispLogical(8)
        , expectedTformOutput = "FFTFT"
        , expectedTdispOutput = "{       F,       F,       T,       F,       T}"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 3: TFORM bit" must "be properly displayed" in {

    assert(
      performTest(rawInput = Array(0xFE.toByte)
        , BinTableTformBit(8)
        , TdispBinary(8, 8)
        , expectedTformOutput = "11111110"
        , expectedTdispOutput = "11111110"
      ) == true)

    assert(
      performTest(rawInput = Array(0x70.toByte)
        , BinTableTformBit(3)
        , TdispBinary(8, 8)
        , expectedTformOutput = "11100000"
        , expectedTdispOutput = "11100000"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte)
        , BinTableTformBit(8)
        , TdispBinary(9, 8)
        , expectedTformOutput = "11011110"
        , expectedTdispOutput = " 11011110"

      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte)
        , BinTableTformBit(8)
        , TdispBinary(4, 4)
        , expectedTformOutput = "11011110"
        , expectedTdispOutput = "1101"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte)
        , BinTableTformBit(8)
        , TdispBinary(16, 10)
        , expectedTformOutput = "11011110"
        , expectedTdispOutput = "      0011011110"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispBinary(1, 1)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{1,1}"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispInteger(5, 3)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{  222,  173}"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispInteger(3, 3)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{222,173}"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispInteger(7, 6)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{ 000222, 000173}"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispInteger(4, 2)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{ 222, 173}"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispInteger(8,2)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{     222,     173}"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispOctal(8, 8)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{00000336,00000255}"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispHexadecimal(5, 5)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{000DE,000AD}"
      ) == true)

    //float display
    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispFloatFixed(12, 8)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{222.00000000,173.00000000}"
      ) == true)

    assert(
      performTest(rawInput =Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispFloatExponential(12, 2, 3)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{   0.22E+003,   0.17E+003}"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispEngineeringExponential(12, 2, 3)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{ 222.00E+000, 173.00E+000}"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispScientificExponential(12, 2, 3)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{   2.22E+002,   1.73E+002}"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispGeneralExponential(12, 2, 3)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{      222.00,      173.00}"
      ) == true)

    assert(
      performTest(rawInput = Array(0xde.toByte,0xad.toByte)
        , BinTableTformBit(16)
        , TdispDoubleExponential(12, 2, 3)
        , expectedTformOutput = "{11011110,10101101}"
        , expectedTdispOutput = "{   0.22D+003,   0.17D+003}"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 4: TFORM unsigned byte" must "be properly displayed" in {

    assert(
      performTest(rawInput = DATA_TYPE_U_INT_8.getByteSeq('A'.toByte)
        , BinTableTformUnsignedByte()
        , TdispBinary(8, 8)
        , expectedTformOutput = "65"
        , expectedTdispOutput = "10000010"
      ) == true)

    assert(
      performTest(rawInput = DATA_TYPE_U_INT_8.getByteSeq('A'.toByte)
        , BinTableTformUnsignedByte()
        , TdispInteger(8, 3)
        , expectedTformOutput = "65"
        , expectedTdispOutput = "     065"
      ) == true)


     assert(
      performTest(rawInput = DATA_TYPE_U_INT_8.getByteSeq(255.toByte)
        , BinTableTformUnsignedByte()
        , TdispInteger(3, 3)
        , expectedTformOutput = "255"
        , expectedTdispOutput = "255"
      ) == true)

    assert(
      performTest(rawInput = Array('G'.toByte,'E'.toByte,'R'.toByte,'O'.toByte,'L'.toByte,'A'.toByte)
        , BinTableTformUnsignedByte(6)
        , TdispInteger(3, 3)
        , expectedTformOutput = "{71,69,82,79,76,65}"
        , expectedTdispOutput = "{071,069,082,079,076,065}"
      ) == true)

    assert(
      performTest(rawInput = Array('G'.toByte,0.toByte,'R'.toByte,'O'.toByte,'L'.toByte,'A'.toByte)
        , BinTableTformUnsignedByte(6)
        , TdispBinary(8, 8)
        , expectedTformOutput = "{71,0,82,79,76,65}"
        , expectedTdispOutput = "{10001110,00000000,10100100,10011110,10011000,10000010}"
      ) == true)

    assert(
      performTest(rawInput = Array('G'.toByte,'E'.toByte,'R'.toByte,'O'.toByte,'L'.toByte,'A'.toByte)
        , BinTableTformUnsignedByte(6)
        , TdispOctal(8, 3)
        , expectedTformOutput = "{71,69,82,79,76,65}"
        , expectedTdispOutput = "{     107,     105,     122,     117,     114,     101}"
      ) == true)

    assert(
      performTest(rawInput = Array('G'.toByte,'E'.toByte,'R'.toByte,'O'.toByte,'L'.toByte,'A'.toByte)
        , BinTableTformUnsignedByte(6)
        , TdispHexadecimal(3, 3)
        , expectedTformOutput = "{71,69,82,79,76,65}"
        , expectedTdispOutput = "{047,045,052,04F,04C,041}"
      ) == true)

    //float display
    assert(
      performTest(rawInput = "0100011101100101".getBytes()
        , BinTableTformBit(16)
        , TdispFloatFixed(12, 8)
        , expectedTformOutput = "{11000000,11000100}"
        , expectedTdispOutput = "{ 48.00000000, 49.00000000}"
      ) == true)

    assert(
      performTest(rawInput = "0100011101100101".getBytes()
        , BinTableTformBit(16)
        , TdispFloatExponential(12, 2, 3)
        , expectedTformOutput = "{11000000,11000100}"
        , expectedTdispOutput = "{   0.48E+002,   0.49E+002}"
      ) == true)

    assert(
      performTest(rawInput = "0100011101100101".getBytes()
        , BinTableTformBit(16)
        , TdispEngineeringExponential(12, 2, 3)
        , expectedTformOutput = "{11000000,11000100}"
        , expectedTdispOutput = "{  48.00E+000,  49.00E+000}"
      ) == true)

    assert(
      performTest(rawInput = "0100011101100101".getBytes()
        , BinTableTformBit(16)
        , TdispScientificExponential(12, 2, 3)
        , expectedTformOutput = "{11000000,11000100}"
        , expectedTdispOutput = "{   4.80E+001,   4.90E+001}"
      ) == true)

    assert(
      performTest(rawInput = "0100011101100101".getBytes()
        , BinTableTformBit(16)
        , TdispGeneralExponential(12, 2, 3)
        , expectedTformOutput = "{11000000,11000100}"
        , expectedTdispOutput = "{    0.48E+02,    0.49E+02}"
      ) == true)

    assert(
      performTest(rawInput = "0100011101100101".getBytes()
        , BinTableTformBit(16)
        , TdispDoubleExponential(12, 2, 3)
        , expectedTformOutput = "{11000000,11000100}"
        , expectedTdispOutput = "{   0.48D+002,   0.49D+002}"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 5: TFORM 16-bit integer" must "be properly displayed" in {

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte)
        , BinTableTformInt_16()
        , TdispInteger(8, 8)
        , expectedTformOutput = "9029"
        , expectedTdispOutput = "00009029"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte,0x45.toByte
                                  ,0x67.toByte,0xFF.toByte)
        , BinTableTformInt_16(2)
        , TdispInteger(8, 8)
        , expectedTformOutput = "{9029,26623}"
        , expectedTdispOutput = "{00009029,00026623}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte,0x45.toByte
                                  ,0x67.toByte,0xFF.toByte)
        , BinTableTformInt_16(2)
        , TdispBinary(16, 16)
        , expectedTformOutput = "{9029,26623}"
        , expectedTdispOutput = "{1000110100010100,1100111111111110}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte,0x45.toByte
                                  ,0x67.toByte,0xFF.toByte)
        , BinTableTformInt_16(2)
        , TdispOctal(8, 3)
        , expectedTformOutput = "{9029,26623}"
        , expectedTdispOutput = "{   21505,   63777}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte,0x45.toByte
                                  ,0x67.toByte,0xFF.toByte)
        , BinTableTformInt_16(2)
        , TdispHexadecimal(4, 4)
        , expectedTformOutput = "{9029,26623}"
        , expectedTdispOutput = "{2345,67FF}"
      ) == true)

    //float display
    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte
                                 , 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_16(2)
        , TdispFloatFixed(12, 8)
        , expectedTformOutput = "{9029,26623}"
        , expectedTdispOutput = "{9029.0000000,26623.000000}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte
                                 , 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_16(2)
        , TdispFloatExponential(12, 2, 3)
        , expectedTformOutput = "{9029,26623}"
        , expectedTdispOutput = "{   0.90E+004,   0.27E+005}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte
                                 , 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_16(2)
        , TdispEngineeringExponential(12, 2, 3)
        , expectedTformOutput = "{9029,26623}"
        , expectedTdispOutput = "{ 902.90E+001, 266.23E+002}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte
                                 , 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_16(2)
        , TdispScientificExponential(12, 2, 3)
        , expectedTformOutput = "{9029,26623}"
        , expectedTdispOutput = "{   9.03E+003,   2.66E+004}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte
                                 , 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_16(2)
        , TdispGeneralExponential(12, 2, 3)
        , expectedTformOutput = "{9029,26623}"
        , expectedTdispOutput = "{     9029.00,    26623.00}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte
                                 , 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_16(2)
        , TdispDoubleExponential(12, 2, 3)
        , expectedTformOutput = "{9029,26623}"
        , expectedTdispOutput = "{   0.90D+004,   0.27D+005}"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 6: TFORM 32-bit integer" must "be properly displayed" in {

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_32()
        , TdispInteger(12, 4)
        , expectedTformOutput = "591751167"
        , expectedTdispOutput = "   591751167"
      ) == true)

   assert(
      performTest(rawInput = Array(0x23.toByte,0x45.toByte,0x67.toByte,0xFF.toByte
                                 , 0xFF.toByte,0xFF.toByte,0x67.toByte,0xFF.toByte)
        , BinTableTformInt_32(2)
        , TdispInteger(12, 4)
        , expectedTformOutput = "{591751167,-38913}"
        , expectedTdispOutput = "{   591751167,      -38913}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_32(2)
        , TdispBinary(32, 32)
        , expectedTformOutput = "{591751167,-38913}"
        , expectedTdispOutput = "{10001101000101011001111111111100,11111111111111111111111111111111}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte
                                  , 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_32(2)
        , TdispOctal(32, 8)
        , expectedTformOutput = "{591751167,-38913}"
        , expectedTdispOutput = "{                      4321263777,                    777777663777}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte
                                  , 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_32(2)
        , TdispHexadecimal(32, 8)
        , expectedTformOutput = "{591751167,-38913}"
        , expectedTdispOutput = "{                        234567FF,                FFFFFFFFFFFF67FF}"
      ) == true)

    //float display
    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_32(2)
        , TdispFloatFixed(12, 8)
        , expectedTformOutput = "{591751167,-38913}"
        , expectedTdispOutput = "{591751167.00,-38913.00000}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_32(2)
        , TdispFloatExponential(12, 2, 3)
        , expectedTformOutput = "{591751167,-38913}"
        , expectedTdispOutput = "{   0.59E+009,  -0.39E+005}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_32(2)
        , TdispEngineeringExponential(12, 2, 3)
        , expectedTformOutput = "{591751167,-38913}"
        , expectedTdispOutput = "{ 591.75E+006,-389.13E+002}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_32(2)
        , TdispScientificExponential(12, 2, 3)
        , expectedTformOutput = "{591751167,-38913}"
        , expectedTdispOutput = "{   5.92E+008,  -3.89E+004}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_32(2)
        , TdispGeneralExponential(12, 2, 3)
        , expectedTformOutput = "{591751167,-38913}"
        , expectedTdispOutput = "{591751167.00,   -38913.00}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_32(2)
        , TdispDoubleExponential(12, 2, 3)
        , expectedTformOutput = "{591751167,-38913}"
        , expectedTdispOutput = "{   0.59D+009,  -0.39D+005}"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 7: TFORM 64-bit integer" must "be properly displayed" in {

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte, 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte)
        , BinTableTformInt_64()
        , TdispInteger(22, 8)
        , expectedTformOutput = "2541551913929762815"
        , expectedTdispOutput = "   2541551913929762815"
      ) == true)

   assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte, 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0x55.toByte, 0x78.toByte, 0x02.toByte, 0xFF.toByte, 0xFF.toByte, 0x75.toByte, 0xFF.toByte)
        , BinTableTformInt_64(2)
        , TdispInteger(22, 8)
        , expectedTformOutput = "{2541551913929762815,-48000266737322497}"
        , expectedTdispOutput = "{   2541551913929762815,    -48000266737322497}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte, 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0x55.toByte, 0x78.toByte, 0x02.toByte, 0xFF.toByte, 0xFF.toByte, 0x75.toByte, 0xFF.toByte)
        , BinTableTformInt_64(2)
        , TdispBinary(64, 12)
        , expectedTformOutput = "{2541551913929762815,-48000266737322497}"
        , expectedTdispOutput = "{1000110100010101100111111111111111111111111111011001111111111100,1111111101010101011110000000001011111111111111110111010111111111}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte, 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0x55.toByte, 0x78.toByte, 0x02.toByte, 0xFF.toByte, 0xFF.toByte, 0x75.toByte, 0xFF.toByte)
        , BinTableTformInt_64(2)
        , TdispOctal(22, 8)
        , expectedTformOutput = "{2541551913929762815,-48000266737322497}"
        , expectedTdispOutput = "{ 215053177777777663777,1775253600137777672777}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte, 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0x55.toByte, 0x78.toByte, 0x02.toByte, 0xFF.toByte, 0xFF.toByte, 0x75.toByte, 0xFF.toByte)
        , BinTableTformInt_64(2)
        , TdispHexadecimal(22, 8)
        , expectedTformOutput = "{2541551913929762815,-48000266737322497}"
        , expectedTdispOutput = "{      234567FFFFFF67FF,      FF557802FFFF75FF}"
      ) == true)

    //float display
    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte, 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0x55.toByte, 0x78.toByte, 0x02.toByte, 0xFF.toByte, 0xFF.toByte, 0x75.toByte, 0xFF.toByte)
        , BinTableTformInt_64(2)
        , TdispFloatFixed(30, 8)
        , expectedTformOutput = "{2541551913929762815,-48000266737322497}"
        , expectedTdispOutput = "{  2541551913929762800.00000000,   -48000266737322496.00000000}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte, 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte
                                  , 0xFF.toByte, 0x55.toByte, 0x78.toByte, 0x02.toByte, 0xFF.toByte, 0xFF.toByte, 0x75.toByte, 0xFF.toByte)
        , BinTableTformInt_64(2)
        , TdispFloatExponential(30, 2, 3)
        , expectedTformOutput = "{2541551913929762815,-48000266737322497}"
        , expectedTdispOutput = "{                     0.25E+019,                    -0.48E+017}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte, 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0x55.toByte, 0x78.toByte, 0x02.toByte, 0xFF.toByte, 0xFF.toByte, 0x75.toByte, 0xFF.toByte)
        , BinTableTformInt_64(2)
        , TdispEngineeringExponential(30, 2, 3)
        , expectedTformOutput = "{2541551913929762815,-48000266737322497}"
        , expectedTdispOutput = "{                   254.16E+016,                  -480.00E+014}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte, 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0x55.toByte, 0x78.toByte, 0x02.toByte, 0xFF.toByte, 0xFF.toByte, 0x75.toByte, 0xFF.toByte)
        , BinTableTformInt_64(2)
        , TdispScientificExponential(30, 2, 3)
        , expectedTformOutput = "{2541551913929762815,-48000266737322497}"
        , expectedTdispOutput = "{                     2.54E+018,                    -4.80E+016}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte, 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0x55.toByte, 0x78.toByte, 0x02.toByte, 0xFF.toByte, 0xFF.toByte, 0x75.toByte, 0xFF.toByte)
        , BinTableTformInt_64(2)
        , TdispGeneralExponential(30, 2, 3)
        , expectedTformOutput = "{2541551913929762815,-48000266737322497}"
        , expectedTdispOutput = "{        2541551913929762800.00,         -48000266737322496.00}"
      ) == true)

    assert(
      performTest(rawInput = Array(0x23.toByte, 0x45.toByte, 0x67.toByte, 0xFF.toByte, 0xFF.toByte, 0xFF.toByte, 0x67.toByte, 0xFF.toByte
                                 , 0xFF.toByte, 0x55.toByte, 0x78.toByte, 0x02.toByte, 0xFF.toByte, 0xFF.toByte, 0x75.toByte, 0xFF.toByte)
        , BinTableTformInt_64(2)
        , TdispDoubleExponential(30, 2, 3)
        , expectedTformOutput = "{2541551913929762815,-48000266737322497}"
        , expectedTdispOutput = "{                     0.25D+019,                    -0.48D+017}"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 8: TFORM character" must "be properly displayed" in {
    assert(
      performTest(rawInput = "vino guerola".getBytes()
        , BinTableTformCharacter(12)
        , TdispCharacter(12)
        , expectedTformOutput = "vino guerola"
        , expectedTdispOutput = "vino guerola"
      ) == true)

    assert(
      performTest(rawInput = "vino guerola".getBytes()
        , BinTableTformCharacter(12)
        , TdispCharacter(13)
        , expectedTformOutput = "vino guerola"
        , expectedTdispOutput = " vino guerola"
      ) == true)


    assert(
      performTest(rawInput = "vino guerola".getBytes()
        , BinTableTformCharacter(12)
        , TdispCharacter(11)
        , expectedTformOutput = "vino guerola"
        , expectedTdispOutput = "vino guerol"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 9: TFORM single precision float point" must "be properly displayed" in {
    assert(
      performTest(rawInput = DATA_TYPE_FLOAT_32.getByteSeq(-1231.67E12f)
        , BinTableTformFloat_32()
        , TdispFloatFixed(30, 8)
        , expectedTformOutput = "-1.23167E15"
        , expectedTdispOutput = "    -1231670000000000.00000000"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_32.getByteSeq(-1231.67E12f)
                                 , DATA_TYPE_FLOAT_32.getByteSeq(12231.6E-2f)).flatten
        , BinTableTformFloat_32(2)
        , TdispFloatFixed(30, 8)
        , expectedTformOutput = "{-1.23167E15,122.316}"
        , expectedTdispOutput = "{    -1231670000000000.00000000,                  122.31600000}"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_32.getByteSeq(-254E4f)
                                 , DATA_TYPE_FLOAT_32.getByteSeq(-250E4f)).flatten
        , BinTableTformFloat_32(2)
        , TdispFloatExponential(12, 2, 3)
        , expectedTformOutput = "{-2540000.0,-2500000.0}"
        , expectedTdispOutput = "{  -0.25E+007,  -0.25E+007}"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_32.getByteSeq(-254E2f)
                                 , DATA_TYPE_FLOAT_32.getByteSeq(-250E2f)).flatten
        , BinTableTformFloat_32(2)
        , TdispEngineeringExponential(12, 2, 3)
        , expectedTformOutput = "{-25400.0,-25000.0}"
        , expectedTdispOutput = "{-254.00E+002,-250.00E+002}"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_32.getByteSeq(-254E1f)
                                 , DATA_TYPE_FLOAT_32.getByteSeq(-250E1f)).flatten
        , BinTableTformFloat_32(2)
        , TdispScientificExponential(12, 2, 3)
        , expectedTformOutput = "{-2540.0,-2500.0}"
        , expectedTdispOutput = "{  -2.54E+003,  -2.50E+003}"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_32.getByteSeq(-1254E1f)
                                 , DATA_TYPE_FLOAT_32.getByteSeq(-121311E1f)).flatten
        , BinTableTformFloat_32(2)
        , TdispGeneralExponential(30, 2, 3)
        , expectedTformOutput = "{-12540.0,-1213110.0}"
        , expectedTdispOutput = "{                     -12540.00,                   -1213110.00}"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_32.getByteSeq(-254E34f)
                                 , DATA_TYPE_FLOAT_32.getByteSeq(-250E14f)).flatten
        , BinTableTformFloat_32(2)
        , TdispDoubleExponential(30, 2, 8)
        , expectedTformOutput = "{-2.54E36,-2.5E16}"
        , expectedTdispOutput = "{               -0.25D+00000037,               -0.25D+00000017}"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 10: TFORM double precision float point" must "be properly displayed" in {
    assert(
      performTest(rawInput = DATA_TYPE_FLOAT_64.getByteSeq(-1231.67E12f)
        , BinTableTformFloat_64()
        , TdispFloatFixed(30, 8)
        , expectedTformOutput = "-1.231670053175296E15"
        , expectedTdispOutput = "    -1231670053175296.00000000"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_64.getByteSeq(-1231.67E12f)
                                 , DATA_TYPE_FLOAT_64.getByteSeq(12231.6E-2f)).flatten
        , BinTableTformFloat_64(2)
        , TdispFloatFixed(30, 8)
        , expectedTformOutput = "{-1.231670053175296E15,122.31600189208984}"
        , expectedTdispOutput = "{    -1231670053175296.00000000,                  122.31600189}"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_64.getByteSeq(-254E4f)
                                 , DATA_TYPE_FLOAT_64.getByteSeq(-250E4f)).flatten
        , BinTableTformFloat_64(2)
        , TdispFloatExponential(12, 2, 3)
        , expectedTformOutput = "{-2540000.0,-2500000.0}"
        , expectedTdispOutput = "{  -0.25E+007,  -0.25E+007}"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_64.getByteSeq(-254E2f)
                                 , DATA_TYPE_FLOAT_64.getByteSeq(-250E2f)).flatten
        , BinTableTformFloat_64(2)
        , TdispEngineeringExponential(12, 2, 3)
        , expectedTformOutput = "{-25400.0,-25000.0}"
        , expectedTdispOutput = "{-254.00E+002,-250.00E+002}"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_64.getByteSeq(-254E1f)
                                 , DATA_TYPE_FLOAT_64.getByteSeq(-250E1f)).flatten
        , BinTableTformFloat_64(2)
        , TdispScientificExponential(12, 2, 3)
        , expectedTformOutput = "{-2540.0,-2500.0}"
        , expectedTdispOutput = "{  -2.54E+003,  -2.50E+003}"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_64.getByteSeq(-1254E1f)
                                 , DATA_TYPE_FLOAT_64.getByteSeq(-121311E1f)).flatten
        , BinTableTformFloat_64(2)
        , TdispGeneralExponential(30, 2, 3)
        , expectedTformOutput = "{-12540.0,-1213110.0}"
        , expectedTdispOutput = "{                     -12540.00,                   -1213110.00}"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_64.getByteSeq(-254E34f)
                                 , DATA_TYPE_FLOAT_64.getByteSeq(-250E14f)).flatten
        , BinTableTformFloat_64(2)
        , TdispDoubleExponential(30, 2, 8)
        , expectedTformOutput = "{-2.5400000643188548E36,-2.4999999607668736E16}"
        , expectedTdispOutput = "{               -0.25D+00000037,               -0.25D+00000017}"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 11: TFORM single precision complex" must "be properly displayed" in {
    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_32.getByteSeq(-254E1f)
                                 , DATA_TYPE_FLOAT_32.getByteSeq(-250E1f)).flatten
        , BinTableTformFloat_32_Complex()
        , TdispFloatFixed(20,3)
        , expectedTformOutput = "{(-2540.0,-2500.0)}"
        , expectedTdispOutput = "(           -2540.000,           -2500.000)"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_32.getByteSeq(-254E1f)
                                 , DATA_TYPE_FLOAT_32.getByteSeq(-250E1f)
                                 , DATA_TYPE_FLOAT_32.getByteSeq(124E21f)
                                 , DATA_TYPE_FLOAT_32.getByteSeq(31E12f)).flatten
        , BinTableTformFloat_32_Complex(2)
        , TdispFloatFixed(20, 3)
        , expectedTformOutput = "{(-2540.0,-2500.0),(1.24E23,3.1E13)}"
        , expectedTdispOutput = "{(           -2540.000,           -2500.000),(12400000000000001000,  31000000000000.000)}"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 12: TFORM double precision complex" must "be properly displayed" in {
   assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_64.getByteSeq(-254E1f)
                                 , DATA_TYPE_FLOAT_64.getByteSeq(-250E1f)).flatten
        , BinTableTformFloat_64_Complex()
        , TdispFloatFixed(20, 3)
        , expectedTformOutput = "{(-2540.0,-2500.0)}"
        , expectedTdispOutput = "(           -2540.000,           -2500.000)"
      ) == true)

    assert(
      performTest(rawInput = Array(DATA_TYPE_FLOAT_64.getByteSeq(-254E1)
                                 , DATA_TYPE_FLOAT_64.getByteSeq(-250E1)
                                 , DATA_TYPE_FLOAT_64.getByteSeq(124E8)
                                 , DATA_TYPE_FLOAT_64.getByteSeq(31E18)).flatten
        , BinTableTformFloat_64_Complex(2)
        , TdispFloatExponential(8, 3)
        , expectedTformOutput = "{(-2540.0,-2500.0),(1.24E10,3.1E19)}"
        , expectedTdispOutput = "{(-0.2E+04,-0.2E+04),(0.12E+11,0.31E+20)}"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 13: TFORM array decriptor 32-bit integer" must "be properly displayed" in {
    assert( true) //superseed by BinTableTformInt_32
  }
  //---------------------------------------------------------------------------
  "Test 14: TFORM array decriptor 64-bit integer" must "be properly displayed" in {
    assert( true) //superseed by BinTableTformInt_64
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BinTableTformTdisp_Spec.scala
//=============================================================================

/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Nov/2022
 * Time:  09h:27m
 * Description: None
 */
//=============================================================================
package standard.table.asciiTable

//=============================================================================
import com.common.fits.standard.fits.FitsCtxt
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.AsciiTableColDef
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.tform._
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.tdisp._
import com.common.util.string.MyString.leftPadding
import common.UnitSpec
//=============================================================================
//=============================================================================
object AsciiTableTformTdisp_Spec {
  //---------------------------------------------------------------------------
  private def performTest(rawInput: String
                          , tForm: AsciiTableTform
                          , tDisp: Tdisp
                          , expectedTformOutput: String
                          , expectedTdispOutput: String
                          , debug: Boolean = true): Boolean = {

    val colDef = AsciiTableColDef(colIndex = 0, position = 0, form = tForm, disp = Some(tDisp))

    //ensure the size of the input. The input comes from a sclice of a table row, so
    // the slice has that has always the proper size
    val input = leftPadding(rawInput.take(tForm.charSize.toInt), tForm.charSize.toInt)
    if (debug) {
      println(s"------------------------------------")
      println(s"input:    '$input' size(${input.size})")
    }

    //format string following tForm
    val tFormString = tForm.format(input, FitsCtxt())
    if (debug) {
      println(s"TFORM:    '$tFormString' size(${tFormString.size})")
      println(s"expected: '$expectedTformOutput' size(${expectedTformOutput.size})")
      println(s"is valid result: '${tFormString == expectedTformOutput}'")
      if (tFormString != expectedTformOutput)
        println("ERROR!")
      println(s"........................................................")
    }
    if (tFormString != expectedTformOutput) return false

    //format string following tDisp
    val tDispString = colDef.format(input)
    if (debug) {
      println(s"TDISP:    '$tDispString' size(${tDispString.size})")
      println(s"expected: '$expectedTdispOutput' size(${expectedTdispOutput.size})")
      println(s"is valid result: '${tDispString == expectedTdispOutput}'}")
      if (tDispString != expectedTdispOutput)
        println("ERROR!")
      println(s"+++++++++++++++++++++++++++++++++++")
    }
    tDispString == expectedTdispOutput
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import AsciiTableTformTdisp_Spec._
class AsciiTableTformTdisp_Spec  extends UnitSpec {

  //---------------------------------------------------------------------------
 "Test 1: TFORM character" must "be properly displayed" in {
    assert(
      performTest(rawInput = " abcd "
      , AsciiTableTformCharacter(8)
      , TdispCharacter(16)
      , expectedTformOutput = "   abcd "
      , expectedTdispOutput = "           abcd "
      ) == true)

    assert(
      performTest(rawInput = "a"
        , AsciiTableTformCharacter(2)
        , TdispCharacter(7)
        , expectedTformOutput = " a"
        , expectedTdispOutput = "      a"
      ) == true)

    assert(
      performTest(rawInput = "\u00B1"
        , AsciiTableTformCharacter(2)
        , TdispCharacter(7)
        , expectedTformOutput = " ±"
        , expectedTdispOutput = "      ±"
      ) == true)  //indeed, it must be invalid due to char '±' must be in the range {0x20, 0x7E}
  }

  //---------------------------------------------------------------------------
  "Test 2: TFORM integer" must "be properly displayed" in {

    assert(
      performTest(rawInput = " 1234 "
        , AsciiTableTformInteger(8)
        , TdispInteger(16,3)
        , expectedTformOutput = "   1234 "
        , expectedTdispOutput = "            1234"
      ) == true)

    assert(
      performTest(rawInput = " 1234 "
        , AsciiTableTformInteger(8)
        , TdispInteger(8, 5)
        , expectedTformOutput = "   1234 "
        , expectedTdispOutput = "   01234"
      ) == true)

    assert(
      performTest(rawInput = " 1234 "
        , AsciiTableTformInteger(8)
        , TdispInteger(3, 3)
        , expectedTformOutput = "   1234 "
        , expectedTdispOutput = "***"
      ) == true)

    assert(
      performTest(rawInput = "1 "
        , AsciiTableTformInteger(2)
        , TdispInteger(7, 2)
        , expectedTformOutput = "1 "
        , expectedTdispOutput = "     01"
      ) == true)

    assert(
      performTest(rawInput = "-9223372036854775808"
        , AsciiTableTformInteger(20)
        , TdispInteger(25, 2)
        , expectedTformOutput = "-9223372036854775808"
        , expectedTdispOutput = "     -9223372036854775808"
      ) == true)

    assert(
      performTest(rawInput = " 9223372036854775807 "
        , AsciiTableTformInteger(22)
        , TdispInteger(25, 2)
        , expectedTformOutput = "  9223372036854775807 "
        , expectedTdispOutput = "      9223372036854775807"
      ) == true)

    assert(
      performTest(rawInput = "234212"
        , AsciiTableTformInteger(6)
        , TdispBinary(24, 2)
        , expectedTformOutput = "234212"
        , expectedTdispOutput = "111001001011100100000000"
      ) == true)


    assert(
      performTest(rawInput = "9223372036854775807"
        , AsciiTableTformInteger(20)
        , TdispBinary(64, 2)
        , expectedTformOutput = " 9223372036854775807"
        , expectedTdispOutput = "1111111111111111111111111111111111111111111111111111111111111110"
      ) == true)

    assert(
      performTest(rawInput = "-9223372036854775808"
        , AsciiTableTformInteger(20)
        , TdispOctal(32, 2)
        , expectedTformOutput = "-9223372036854775808"
        , expectedTdispOutput = "          1000000000000000000000"
      ) == true)

    assert(
      performTest(rawInput = "-9223372036854775808"
        , AsciiTableTformInteger(20)
        , TdispHexadecimal(16, 2)
        , expectedTformOutput = "-9223372036854775808"
        , expectedTdispOutput = "8000000000000000"
      ) == true)

    assert(
      performTest(rawInput = "-9223372036854775808"
        , AsciiTableTformInteger(20)
        , TdispFloatFixed(40, 8)
        , expectedTformOutput = "-9223372036854775808"
        , expectedTdispOutput = "           -9223372036854776000.00000000"
      ) == true)

    assert(
      performTest(rawInput = "-9223372036854775808"
        , AsciiTableTformInteger(20)
        , TdispFloatExponential(10, 3, 2)
        , expectedTformOutput = "-9223372036854775808"
        , expectedTdispOutput = "-0.922E+19"
      ) == true)

    assert(
      performTest(rawInput = "-9223372036854775808"
        , AsciiTableTformInteger(20)
        , TdispFloatExponential(32, 2, 6)
        , expectedTformOutput = "-9223372036854775808"
        , expectedTdispOutput = "                   -0.92E+000019"
      ) == true)

    assert(
      performTest(rawInput = "-9223372036854775808"
        , AsciiTableTformInteger(20)
        , TdispEngineeringExponential(32, 4, 3)
        , expectedTformOutput = "-9223372036854775808"
        , expectedTdispOutput = "                  -922.3372E+016"
      ) == true)

    assert(
      performTest(rawInput = "1.4234E234"
        , AsciiTableTformInteger(20)
        , TdispEngineeringExponential(32, 4, 2)
        , expectedTformOutput = "          1.4234E234"
        , expectedTdispOutput = "********************************"
      ) == true)

    assert(
      performTest(rawInput = "-9223372036854775808"
        , AsciiTableTformInteger(20)
        , TdispScientificExponential(32, 4, 4)
        , expectedTformOutput = "-9223372036854775808"
        , expectedTdispOutput = "                   -9.2234E+0018"
      ) == true)

    assert(
      performTest(rawInput = (0.1 - 0.5 * Math.pow(10,3)).toString
        , AsciiTableTformInteger(20)
        , TdispGeneralExponential(20, 4, 4)
        , expectedTformOutput = "              -499.9"
        , expectedTdispOutput = "         -0.4999E+03"
      ) == true)

    assert(
      performTest(rawInput =  (Math.pow(10, 4) - 0.5 + 0.001).toString
        , AsciiTableTformInteger(20)
        , TdispGeneralExponential(20, 4, 4)
        , expectedTformOutput = "            9999.501"
        , expectedTdispOutput = "           9999.5010"
      ) == true)

    assert(
      performTest(rawInput = (Math.pow(10, 2) * (1 - 0.5 * Math.pow(10,-4))).toString
        , AsciiTableTformInteger(20)
        , TdispGeneralExponential(20, 4, 4)
        , expectedTformOutput = "              99.995"
        , expectedTdispOutput = "          1.0000E+02"
      ) == true)

    assert(
      performTest(rawInput = "-9223372036854775808"
        , AsciiTableTformInteger(20)
        , TdispDoubleExponential(10, 3, 2)
        , expectedTformOutput = "-9223372036854775808"
        , expectedTdispOutput = "-0.922D+19"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 3: TFORM float fixed number" must "be properly displayed" in {
    assert(
      performTest(rawInput = "3.14"
        , AsciiTableTformFloatFixed(8, 4)
        , TdispCharacter(20)
        , expectedTformOutput = "  3.1400"
        , expectedTdispOutput = "                3.14"
      ) == true)

    assert(
      performTest(rawInput = "3.14"
        , AsciiTableTformFloatFixed(8, 4)
        , TdispFloatFixed(20, 4)
        , expectedTformOutput = "  3.1400"
        , expectedTdispOutput = "              3.1400"
      ) == true)

    assert(
      performTest(rawInput = "3.14"
        , AsciiTableTformFloatFixed(8, 4)
        , TdispFloatExponential(20, 4, 4)
        , expectedTformOutput = "  3.1400"
        , expectedTdispOutput = "        0.3140E+0001"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloatFixed(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = " -3.1400"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloatFixed(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = " -3.1400"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloatFixed(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = " -3.1400"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloatFixed(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = " -3.1400"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 4: TFORM float 32" must "be properly displayed" in {

    assert(
      performTest(rawInput = "3.14"
        , AsciiTableTformFloat_32(8, 4)
        , TdispCharacter(20)
        , expectedTformOutput = " 3.E0000"
        , expectedTdispOutput = "                3.14"
      ) == true)

    assert(
      performTest(rawInput = "3.14"
        , AsciiTableTformFloat_32(8, 4)
        , TdispFloatFixed(20, 4)
        , expectedTformOutput = " 3.E0000"
        , expectedTdispOutput = "              3.1400"
      ) == true)

    assert(
      performTest(rawInput = "3.14"
        , AsciiTableTformFloat_32(8, 4)
        , TdispFloatExponential(20, 4, 4)
        , expectedTformOutput = " 3.E0000"
        , expectedTdispOutput = "        0.3140E+0001"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloat_32(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = "-3.E0000"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloat_32(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = "-3.E0000"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloat_32(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = "-3.E0000"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloat_32(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = "-3.E0000"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)
  }
  //---------------------------------------------------------------------------
  "Test 5: TFORM float 64" must "be properly displayed" in {

    assert(
      performTest(rawInput = "3.14"
        , AsciiTableTformFloat_64(8, 4)
        , TdispCharacter(20)
        , expectedTformOutput = " 3.E0000"
        , expectedTdispOutput = "                3.14"
      ) == true)

    assert(
      performTest(rawInput = "3.14"
        , AsciiTableTformFloat_64(8, 4)
        , TdispFloatFixed(20, 4)
        , expectedTformOutput = " 3.E0000"
        , expectedTdispOutput = "              3.1400"
      ) == true)

    assert(
      performTest(rawInput = "3.14"
        , AsciiTableTformFloat_64(8, 4)
        , TdispFloatExponential(20, 4, 4)
        , expectedTformOutput = " 3.E0000"
        , expectedTdispOutput = "        0.3140E+0001"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloat_64(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = "-3.E0000"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloat_64(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = "-3.E0000"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloat_64(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = "-3.E0000"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)

    assert(
      performTest(rawInput = "-3.14"
        , AsciiTableTformFloat_64(8, 4)
        , TdispEngineeringExponential(16, 4, 3)
        , expectedTformOutput = "-3.E0000"
        , expectedTdispOutput = "    -3.1400E+000"
      ) == true)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BinTableTformTdisp_Spec.scala
//=============================================================================

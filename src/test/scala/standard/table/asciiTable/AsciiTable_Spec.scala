/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  22/Nov/2022
 * Time:  09h:27m
 * Description: None
 */
//=============================================================================
package standard.table.asciiTable

//=============================================================================
import com.common.fits.standard.block.record.RecordCharacter.RecordString
import com.common.fits.standard.block.record.RecordNumber.RecordInteger
import com.common.fits.standard.Keyword._
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.sectionName.SectionName
import com.common.fits.standard.structure.hdu.extension.conforming.standard.asciiTable.{AsciiTable, AsciiTableRow}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.{CommonTable, TableColDef}
import common.UnitSpec
//=============================================================================
//=============================================================================
class AsciiTable_Spec  extends UnitSpec {
   //---------------------------------------------------------------------------
  "Test 1: an ASCII table well defined with all types of columns" must "be property managed" in {
    //-------------------------------------------------------------------------
    val recordMap = RecordMap()
    //-------------------------------------------------------------------------
    var colIndex   = 1L //index of the column, starting with index 1
    var startingPos = 1L //offset of the column in characters
    //-------------------------------------------------------------------------
    //decimal integer
    var fieldSize = TableColDef.filldMap(
       recordMap
     , colIndex
     , startingPos
     , form = "I10"
     , name = Some("Col_Integer")
     , unit =  Some("km.s")
     , scale = Some(1.3d)
     , zero = Some(13)
     , nullValue = Some("-999")
     , disp = Some("Z8.3")
     , tdMin = Some(-300)
     , tdMax = Some(500)
     , tlMin = Some(-100)
     , tlMax = Some(400)
     , isAsciiTable = true
    )
    if (recordMap.isEmpty) assert(false)
    startingPos += fieldSize + 1 //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    //character
    fieldSize = TableColDef.filldMap(
       recordMap
      , colIndex
      , startingPos
      , form = "A1"
      , isAsciiTable = true)
    startingPos += fieldSize + 1
    colIndex += 1
    //-------------------------------------------------------------------------
    //float point fixed decimal
    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , startingPos
      , form = "F8.3"
      , name = Some("Column float point fixed")
      , isAsciiTable = true
    )
    startingPos += fieldSize + 1  //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    //float point fixed exponential notation, single precision
    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , startingPos
      , form = "E10.5"
      , name = Some("Column float point, exponential notation (float)")
      , isAsciiTable = true
    )
    startingPos += fieldSize + 1  //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    //float point fixed exponential notation, double precision
    fieldSize = TableColDef.filldMap(
      recordMap
      , colIndex
      , startingPos
      , form = "D12.6"
      , name = Some("Column float point, exponential notation (double)")
      , isAsciiTable = true
    )
    startingPos += fieldSize + 1  //one more for the column divider
    colIndex += 1
    //-------------------------------------------------------------------------
    //TFIELDS (mandatory,column count)
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, colIndex - 1))
    //-------------------------------------------------------------------------
    //row values
    val rowValueSeq = Array(
      //-------------------------------------------------------------------------
      //          | |        |          |                  //expected result
      //123456789012345678901234567890123456789012345
      ("       4   A   7.8     3e-2      -2D+23      ", 1 ,Array("     012", "A", "   7.800", "30.E-00003", "-200.E000021"))
    , ("       4   B   4       5            5        ", 1 ,Array("     012", "B", "   0.004", "50.E-00006", "500.E-000008"))
    , ("      -9   C   8       9            1        ", 1 ,Array("     001", "C", "   0.008", "90.E-00006", "100.E-000008"))
    , ("     -400  D   8       9            1        ", 0, Array("    -400", "D", "   0.008", "90.E-00006", "100.E-000008")) //invalid TMIN
    , ("     600   E   8       9            1        ", 0, Array("     600", "E", "   0.008", "90.E-00006", "100.E-000008")) //invalid TMAX
    , ("     -200  F   8       9            1        ", 1, Array("FFFFFF09", "F", "   0.008", "90.E-00006", "100.E-000008")) //invalid TLMIN
    , ("     100   G   8       9            1        ", 1, Array("     08F", "G", "   0.008", "90.E-00006", "100.E-000008")) //invalid TLMAX
    , ("                                             ", 0 ,Array("")) //empty string
    , (""                                             , 0 ,Array("")) //empty string
    )
    //-------------------------------------------------------------------------
    //build column definition
    val colDefSeq = TableColDef.toAsciiTableColDef(
      TableColDef.buildColDefSeq(recordMap,isAciiTable = true,SectionName.SECTION_7_2_2).get)
    //-------------------------------------------------------------------------
    //process all row values
    rowValueSeq.zipWithIndex.foreach { case ((row,expectedResult,expectedValueSeq),processedRowCount) =>

      println(s"Testing:'$row'")
      val (result,rowValueSeq) = AsciiTableRow.getRowValueSeq(row.getBytes()
                                                              , colDefSeq
                                                              , processedRowCount + 1
                                                              , applyLinearTransformation = true)

      if (result && expectedResult == 0) assert(false)
      if (!result && expectedResult == 1) assert(false)

      if (result) {
        expectedValueSeq.zipWithIndex.foreach { case (expectedValue, i) =>
          assert(rowValueSeq(i) == expectedValue)
        }
      }
    }
    assert(true)
  }

  //---------------------------------------------------------------------------
  //fields from 'XTENSION' to 'GCOUNT' super seed by Extension.isValidHeader
  "Test 2: an ASCII table with a wrong NAXIS_1 definition" must "be property managed" in {

    val recordMap = RecordMap()

    recordMap.append(RecordInteger(KEYWORD_NAXIS, 2))
    recordMap.append(RecordInteger(KEYWORD_NAXIS + 1, 2)) //size of each row in characters
    recordMap.append(RecordInteger(KEYWORD_NAXIS + 2, 3)) //row count

    recordMap.append(RecordString(KEYWORD_TFORM + 1, "I1"))
    recordMap.append(RecordString(KEYWORD_TFORM + 2, "A1"))
    recordMap.append(RecordString(KEYWORD_TFORM + 3, "F4.2"))

    assert(AsciiTable.isValidNaxis_1(recordMap) == false)  //expected size is 2 (KEYWORD_NAXIS1), but stored is 6 (due to TFORM definition)
  }
  //---------------------------------------------------------------------------
  "Test 3: an ASCII table with a wrong TFIELDS definition" must "be property managed" in {

    val recordMap = RecordMap()

    recordMap.append(RecordInteger(KEYWORD_TFIELDS, -1))  //must be in range [0,999]
    assert(CommonTable.isValidTfields(recordMap) == false)
    recordMap.remove(KEYWORD_TFIELDS)

    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1000))   //must be in range [0,999]
    assert(CommonTable.isValidTfields(recordMap) == false)
    recordMap.remove(KEYWORD_TFIELDS)

    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 0))
    assert(CommonTable.isValidTfields(recordMap) == true)
    recordMap.remove(KEYWORD_TFIELDS)

    recordMap.append(RecordString(KEYWORD_TFIELDS, "0"))
    assert(CommonTable.isValidTfields(recordMap) == false)   //must be an integer
    recordMap.remove(KEYWORD_TFIELDS)

    assert(true)
  }

  //---------------------------------------------------------------------------
  "Test 4: an ASCII table with a wrong TFORM definition" must "be property managed" in {
    //see tests in 'AsciiTableTformTdisp_Spec'
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 5: an ASCII table with a wrong TBCOL definition" must "be property managed" in {

    val recordMap = RecordMap()

    recordMap.append(RecordInteger(KEYWORD_NAXIS, 2))
    recordMap.append(RecordInteger(KEYWORD_NAXIS + 1, 1))  //size of each row in characters
    recordMap.append(RecordInteger(KEYWORD_NAXIS + 2, 1))   //row count
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1))     //column per row

    recordMap.append(RecordString(KEYWORD_TBCOL + 1, "1"))
    assert(AsciiTable.isValidTbcol(recordMap) == false)   //must be an integer
    recordMap.remove(KEYWORD_TBCOL + 1)

    assert(AsciiTable.isValidTbcol(recordMap) == false) //TBCOL1 must exist

    recordMap.append(RecordInteger(KEYWORD_TBCOL + 0, 1))
    assert(AsciiTable.isValidTbcol(recordMap) == false) //TBCOL1 must exist
    recordMap.remove(KEYWORD_TBCOL + 0)

    recordMap.append(RecordInteger(KEYWORD_TBCOL + 2, 1))
    assert(AsciiTable.isValidTbcol(recordMap) == false) //TBCOL1 must exist
    recordMap.remove(KEYWORD_TBCOL + 2)

    recordMap.append(RecordInteger(KEYWORD_TBCOL + 1, 1))
    recordMap.append(RecordString(KEYWORD_TFORM + 1, "A1"))
    assert(AsciiTable.isValidTbcol(recordMap) == true) //TBCOL1 must exist and its start + char size must fit in the size of the row
    recordMap.remove(KEYWORD_TBCOL + 1)
    recordMap.remove(KEYWORD_TFORM + 1)

    recordMap.append(RecordInteger(KEYWORD_TBCOL + 1, 1))
    recordMap.append(RecordString(KEYWORD_TFORM + 1, "A1"))
    assert(AsciiTable.isValidTbcol(recordMap) == true) //TBCOL1 must exist and its start + char size must fit in the size of the row
    recordMap.remove(KEYWORD_TBCOL + 1)

    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 6: an ASCII table with a wrong TTYPE definition" must "be property managed" in {
    //see 'Test_1' in this class
    //code inspection 'AsciiTable.isValidHeader'
    val recordMap = RecordMap()
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 1))     //column per row
    recordMap.append(RecordString(KEYWORD_TTYPE + 1, "_"))
    CommonTable.checkTtypeCharacterRange(recordMap, SectionName.SECTION_7_2_2)

    //check duplicated
    recordMap.clear()
    recordMap.append(RecordInteger(KEYWORD_TFIELDS, 2))
    recordMap.append(RecordString(KEYWORD_TBCOL + 1, "col_a"))
    recordMap.append(RecordString(KEYWORD_TTYPE + 1, "a"))
    recordMap.append(RecordString(KEYWORD_TFORM + 1, "1"))

    recordMap.append(RecordString(KEYWORD_TBCOL + 2, "col_a"))
    recordMap.append(RecordString(KEYWORD_TTYPE + 2, "A"))
    recordMap.append(RecordString(KEYWORD_TFORM + 2, "2"))
    val r = TableColDef.buildColDefSeq(recordMap
      , isAciiTable = true
      , SectionName.SECTION_7_2_2)

    assert(r.isEmpty)
  }
  //---------------------------------------------------------------------------
  "Test 7: an ASCII table with a wrong TUNIT definition" must "be property managed" in {
    //see 'Test_1' in class 'PhysicalUnitSpec'
    //code inspection 'AsciiTable.isValidHeader'
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 8: an ASCII table with a wrong TSCAL definition" must "be property managed" in {
    //see 'Test_1' in this class
    //code inspection 'AsciiTable.isValidHeader'
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 9: an ASCII table with a wrong TZERO definition" must "be property managed" in {
    //see 'Test_1' in this class
    //code inspection 'AsciiTable.isValidHeader'
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 10: an ASCII table with a wrong TNULL definition" must "be property managed" in {
    //see 'Test_1' in this class
    //code inspection 'AsciiTable.isValidHeader'
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 11: an ASCII table with a wrong TDISP definition" must "be property managed" in {
    //code inspection CommonTable.isValidTdisp
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 12: an ASCII table with a wrong TDMIN definition" must "be property managed" in {
    //code inspection: TableColDef.isValueInRange
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 13: an ASCII table with a wrong TDMAX definition" must "be property managed" in {
    //code inspection: TableColDef.isValueInRange
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 14: an ASCII table with a wrong TLMIN definition" must "be property managed" in {
    //code inspection: TableColDef.isValueInRange
    assert(true)
  }
  //---------------------------------------------------------------------------
  "Test 15: an ASCII table with a wrong TLMAX definition" must "be property managed" in {
    //code inspection: TableColDef.isValueInRange
    assert(true)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file AsciiTable_Spec.scala
//=============================================================================

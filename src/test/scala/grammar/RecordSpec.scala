/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/mar/2018
 * Time:  11h:41m
 * Description: Unit testing for FITS parserTform of keyword + values records
 * The parserTform implements the rules of the grammar defined in appendix A of FITS standard version 4.0 (2018 August 13)
 */
//=============================================================================
package grammar
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.standard.block.record.RecordCharacter.{RecordError, RecordUnknownKeywordValue}
import com.common.fits.standard.block.record.parser.RecordParser
import com.common.fits.standard.ItemSize.RECORD_CHAR_SIZE
import com.common.fits.standard.block.record.RecordMap
import com.common.fits.standard.disconformity.DisconfFatal
import com.common.fits.standard.sectionName.SectionName
import com.common.logger.MyLogger
import com.common.util.string.MyString.rightPadding
//=============================================================================
import common.UnitSpec
//=============================================================================
object RecordSpec extends MyLogger {
  //---------------------------------------------------------------------------
  val recordMap = RecordMap()
  //---------------------------------------------------------------------------
  def parseRecordPair(recordPairSeq: Array[(Boolean,String)]): Boolean = {
    recordPairSeq.foreach { case (expectedValue,record) =>
      info(s"Parsing:'$record' expected to be valid: $expectedValue")
      if (record.length != RECORD_CHAR_SIZE) return false
      RecordParser.run(record) match {
        case _: RecordError =>
          if (expectedValue != false)
            return false
        case r: RecordUnknownKeywordValue =>
          DisconfFatal("", r.comment, SectionName.SECTION_NONE, Array(r.value))
          return false
        case r =>
          println(recordMap.size)
          recordMap.append(r)
      }
    }
    true
  }
}
//-----------------------------------------------------------------------------
import RecordSpec._
class RecordSpec extends UnitSpec {
  //---------------------------------------------------------------------------
  "SectionName 4.2.1.1 single record string keywords: nulls" must "generate a valid record" in {
    MyConf.c = MyConf()
    recordMap.clear()
    val recordPairSeq = Array(
        (true,  rightPadding("KEYWORD0= 'O''HARA'",RECORD_CHAR_SIZE))
      , (false, rightPadding("KEYWORD1= 'O'''HARA'",RECORD_CHAR_SIZE))
      , (true,  rightPadding("KEYWORD2= '' / null string keyword",RECORD_CHAR_SIZE))
      , (true,  rightPadding("KEYWORD3= '' / null string keyword",RECORD_CHAR_SIZE))
      , (true,  rightPadding("KEYWORD4= '' / null string keyword",RECORD_CHAR_SIZE))
    )
    assert(parseRecordPair(recordPairSeq))
  }
  //---------------------------------------------------------------------------
  "SectionName 4.2.1.1 single record string keywords: long-string test 1" must "generate a valid record" in {

    recordMap.clear()
    val recordPairSeq = Array(
        (true, rightPadding("WEATHER = 'Partly cloudy during the evening f&'", RECORD_CHAR_SIZE))
      , (true, rightPadding("CONTINUE 'ollowed by cloudy skies overnight.&'", RECORD_CHAR_SIZE))
      , (true, rightPadding("CONTINUE ' Low 21C. Winds NNE at 5 to 10 mph.'", RECORD_CHAR_SIZE))
    )

    assert(parseRecordPair(recordPairSeq))
  }
  //---------------------------------------------------------------------------
  "SectionName 4.2.1.1 single record string keywords: long-string test 2" must "generate a valid record" in {

    recordMap.clear()
    val recordPairSeq = Array(
        (true, rightPadding("STRKEY  = 'This keyword value is continued &'", RECORD_CHAR_SIZE))
      , (true, rightPadding("CONTINUE ' over multiple keyword records.&'", RECORD_CHAR_SIZE))
      , (true, rightPadding("CONTINUE '&' / The comment field for this", RECORD_CHAR_SIZE))
      , (true, rightPadding("CONTINUE '&' / keyword is also continued", RECORD_CHAR_SIZE))
      , (true, rightPadding("CONTINUE '' / over multiple records.", RECORD_CHAR_SIZE))
    )
    assert(parseRecordPair(recordPairSeq))
  }

  //---------------------------------------------------------------------------
  "SectionName HIERARCH keyword" must "be properly processed" in {

    recordMap.clear()

    //https://fits.gsfc.nasa.gov/registry/hierarch/hierarch.pdf
    val recordPairSeq = Array(
      (true, rightPadding("SIMPLE  =                    T          / Standard FITS format (NOST-100.0)", RECORD_CHAR_SIZE))
      , (true, rightPadding("HIERARCH ESO TEL FOCU SCALE = 1.489 / (deg/m) Focus length = 5.36''mm", RECORD_CHAR_SIZE))
      , (true, rightPadding("HIERARCH ESO INS OPTI-3 ID = 'ESO#427 ' / Optical element identifier", RECORD_CHAR_SIZE))
    )
    assert(parseRecordPair(recordPairSeq))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file ParserRecordSpec.scala
//=============================================================================

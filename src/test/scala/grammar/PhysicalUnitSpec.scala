/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/mar/2018
 * Time:  11h:41m
 * Description: Unit testing for FITS parserTform of keyword + values records
 * The parserTform implements the rules of the grammar defined in appendix A of FITS standard version 4.0 (2018 August 13)
 */
//=============================================================================
package grammar

//=============================================================================
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.unit.parser.ParserPhysicalUnit
//=============================================================================
import common.UnitSpec

//=============================================================================
class PhysicalUnitSpec extends UnitSpec {
  //---------------------------------------------------------------------------
  "Test 1: a list of physical units, valid and invalid" must "generate the expected result" in {
    //(physical unit, expected result) //comment

    val physicalUnitSeq = Seq[(String,Int)](

      //Test case reused and extended from "https://nxg.me.uk/dist/unity/" DOI 10.5281/zenodo.6949817
      ("Hz", 1)
      , ("m", 1)
      , ("1", 0) // no dimensionless symbol
      , ("", 0) // empty string should fail to parse
      , ("n", 0) // unknown physical unit
      , ("Pa", 1)
      , ("Ga", 1)
      , ("Pyr", 1)
      , ("da", 0) //error: interpreted as two consecutive physical units: 'd' and 'a'
      , ("cd", 1)
      , ("dazzle", 0) //unknown physical unit
      , ("wiggle/week", 0) // unknown physical unit
      , ("Marcsec", 0) //not allowed prefix/suffix
      , ("mas", 1)
      , ("mmas", 0) //not allowed prefix/suffix
      , ("pixel/s", 1)
      , ("pixe", 0) //unknown physical unit
      , ("pix/s", 1)
      , ("erg/s", 1)
      , ("cy", 0) //unknown physical unit. Two prefixes, no unit
      , ("Ym", 1)
      , ("Zm", 1)
      , ("Em", 1)
      , ("Pm", 1)
      , ("Tm", 1)
      , ("Gm", 1)
      , ("Mm", 1)
      , ("km", 1)
      , ("hm", 1)
      , ("dam", 1)
      , ("dm", 1)
      , ("cs", 1)
      , ("ms", 1)
      , ("us", 1)
      , ("ns", 1)
      , ("ps", 1)
      , ("fs", 1)
      , ("as", 0)  //not allowed: consecutive units
      , ("zs", 1)
      , ("ys", 1)
      , ("KW", 0)  //error: invalid multlier
      , ("dam dB2", 0) //error: unknow B
      , ("dam.dB**2", 0) //error: unknow B
      , ("mdB", 0)  //error: unknow B
      , ("m kg", 1)
      , ("m*kg", 1)
      , ("(m*kg)*s", 1)
      , ("m.kg", 1)

      //additional tests for physical units
      , ("mm", 1)
      , ("m", 1)
      , ("hm", 1)
      , ("yyr", 1)
      , ("mas", 1)
      , ("mK", 1)
      , ("(m.s.(kA))", 1)
      , ("m.kA rad*sr mm", 1)
      , ("m.(A.km)*rad mm (mol)", 1)
      , ("m.(A.km)*rad mm (mol*(s*m))", 1)
      , ("log((mol*(s*m)))", 1)
      , ("m.log(s)", 1)
      , ("(m)*s", 1)
      , ("((m))*s", 1)

      //additional test for compound physical units
      , ("a", 1)
      , ("s", 1)
      , ("a/s", 1)
      , ("a*s", 1)
      , ("a*s/m/d", 1)
      , ("a*s/m", 1)
      , ("(a", 0)  //error: unbalaced parenthesis
      , ("a)", 0)  //error: unbalaced parenthesis
      , ("(a)", 1)
      , ("(a/s)", 1)
      , ("(a/s*m)", 1)
      , ("(a/(s*m))", 1)
      , ("(a/(s*(m/d)))", 1)
      , ("(a/s)*m", 1)
      , ("(a/s)*(m*d)", 1)
      , ("((a/s)*(m*d))", 1)
      , ("((a/s)*(m*d))/a", 1)
      , ("((a/s)/a", 0)        //error: unbalaced parenthesis
      , ("log((a/s)/a)", 1)
      , ("log((a/s)/exp(d))", 1)
      , ("a/log(s)", 1)
      , ("(a)/log(s)", 1)
      , ("log(a)/log(s)", 1)
      , ("(log(a)/log(s*m))/log(s)", 1)
      , ("a**2", 1)
      , ("a^323", 1)
      , ("a3", 1)
      , ("a**(2)", 1)
      , ("a**+2", 1)
      , ("a+2", 1)
      , ("a2", 1)
      , ("a^2", 1)
      , ("a^(+2)", 1)
      , ("a**-3", 1)
      , ("a-3", 1)
      , ("a^(-3)", 1)
      , ("a3", 1) //originally was "/a3" but it not allowed in the syntax
      , ("a(1.5)", 1)
      , ("a^(1.5)", 1)
      , ("a**(1.5)", 1)
      , ("a(3/2)", 1)
      , ("a**(3/2)", 1)
      , ("a^(3/2)", 1)
      , ("(log(a^(3/2))/log(s*m))/log(s)", 1)
      , ("a/2", 0) //error not allowed
      , ("a1.5.", 0) //error  not allowed
      , ("10**(46)a", 1)
      , ("10^(-46)a", 1)
      , ("10^-46a", 1)
      , ("1^-46a", 0) //error not allowed multiplier 1
      , ("20^-46a", 0) //error not allowed  multiplier 20
      , ("10" + "\u00B1" + "(-46)a", 1)
      , ("10" + "\u00B1" + "+8a", 1)
      , ("10" + "\u00B1" + "-8a", 1)
      , ("10" + "\u00B1" + "(8)a", 1)
      , ("10" + "\u00B1" + "8a", 0) //error not allowed: it must be enclosed
      , ("sqrt(a/s*m/d)", 1)
      , ("10^(46)sqrt(a/s*m/(log(a^(3/2))/log(s*m))/log(s))", 1)
      , ("sqrt(erg/(pixel.s.GHz))", 1)
      , ("ZYeV", 0) //error not allowed: two consecutive multiplier prefix
    )


    //singe case
    //val kk =  ParserPhysicalUnit.run("dam.dB**2")

    val reportAllResults = true
    physicalUnitSeq.foreach { case (s, expectedResult) =>
      val r = ParserPhysicalUnit.parseInput(s)
      if (expectedResult == 1 && r._1.isDefined) {
        if (reportAllResults) println(s"Expected result OK achivied on test case '$s' syntetized: '${r._1.get.synthesizeString}'")
        assert(true)
      }
      else {
        if (expectedResult == 0 && r._1.isEmpty) {
          if (reportAllResults) println(s"Expected result BAD achivied on test case '$s'")
          assert(true)
        }
        else {
          println(s"NO EXPECTED result on test case '$s' Expected: $expectedResult ")
          assert( false)
        }
      }
    }

    assert(true)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file ParserRecordSpec.scala
//=============================================================================

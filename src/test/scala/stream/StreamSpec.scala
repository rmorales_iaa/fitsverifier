/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/mar/2018
 * Time:  11h:41m
 * Description: Unit testing for FITS parserTform of keyword + values records
 * The parserTform implements the rules of the grammar defined in appendix A of FITS standard version 4.0 (2018 August 13)
 */
//=============================================================================
package stream
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.standard.fits.{FitsLoad}
import common.UnitSpec
//=============================================================================
class StremSpec extends UnitSpec {
  //---------------------------------------------------------------------------
  "u FITS file (stream)" must "be loaded from a local storage" in {

    MyConf.c = MyConf()

    //val s = "/home/rafa/proyecto/scalaFitsVerifier/src/test/data/u.fits"
    val s = "/home/rafa/proyecto/scalaFitsVerifier/output/spec/fits_files/1.fits"
   //val s = "/home/rafa/Downloads/work_on_users/dagmara/LaPalma/Mauna_Kea_UH_original/20191001_UH88/Lapalma.003.fit"


    val fits = FitsLoad.load(s, printDisconfFlag = true).get

    val recordSeq  = fits.getRecordSeq

    fits.saveLocal("output/deleteme2.fits")


    assert(true)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file StremSpec.scala
//=============================================================================

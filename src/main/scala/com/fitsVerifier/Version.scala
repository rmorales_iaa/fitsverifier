/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  18/Nov/2022
 * Time:  14h:57m
 * Description: None
 */
//=============================================================================
package com.fitsVerifier
//=============================================================================
import BuildInfo.BuildInfo
//=============================================================================
//=============================================================================
object Version {
  //---------------------------------------------------------------------------
  val value = s"""
      |${BuildInfo.description}
      |\tversion      : ${BuildInfo.version}
      |\tcompilation  : ${BuildInfo.buildInfoBuildNumber}
      |\tauthor       : ${BuildInfo.authorList}
      |\torganization : ${BuildInfo.organization}
      |\thomepage     : ${BuildInfo.homepage.get}
      |\tlicense      : ${BuildInfo.license}
      |\tDetailed build info:
      |\t\tbuild time      : ${BuildInfo.builtAtString}
      |\t\tsbt version     : ${BuildInfo.sbtVersion}
      |\t\tscala version   : ${BuildInfo.scalaVersion}
      |\t\tgit branch      : ${BuildInfo.myGitCurrentBranch}
      |\t\tgit head commit : ${BuildInfo.myGitHeadCommit}""".stripMargin
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Version.scala
//=============================================================================

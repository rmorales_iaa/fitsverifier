/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  03/dic/2018
  * Time:  01h:38m
  * Description: https://github.com/scallop/scallop
  */
//=============================================================================
package com.fitsVerifier.commandLine
//=============================================================================
import com.common.util.file.MyFile.fileExist
import com.common.util.path.Path.directoryExist
import com.fitsVerifier.Version
import org.rogach.scallop._
//=============================================================================
//=============================================================================
object CommandLineParser {
  //-----------------------------------F----------------------------------------
  final val COMMAND_VERIFY_FILE                 = "verify-file"
  final val COMMAND_VERIFY_DIR                  = "verify-dir"
  final val COMMAND_VERIFY_PHYSICAL_UNIT        = "verify-physical-unit"
  final val COMMAND_DUMP                        = "dump"
  final val COMMAND_SLICE_IMAGE_CUBE            = "slice-image-cube"
  final val COMMAND_FORMAT_TABLE_COLUMN         = "format-table-column"
  final val COMMAND_MEF                         = "mef"
  final val COMMAND_BUILD_TABLE_CSV             = "build-table-csv"
  final val COMMAND_VERSION                     = "version"
  //---------------------------------------------------------------------------
  final val VALID_COMMAND_SEQ = Seq(
    COMMAND_VERIFY_FILE
  , COMMAND_VERIFY_DIR
  , COMMAND_VERIFY_PHYSICAL_UNIT
  , COMMAND_DUMP
  , COMMAND_SLICE_IMAGE_CUBE
  , COMMAND_FORMAT_TABLE_COLUMN
  , COMMAND_BUILD_TABLE_CSV
  , COMMAND_VERSION)
  //---------------------------------------------------------------------------
}
//=============================================================================
import CommandLineParser._
class CommandLineParser(args: Array[String]) extends ScallopConf(args) {
  version(Version.value + "\n")
  banner(s"""fitsVerifier syntax
           |java -jar fitsVerifier.jar [--configuration-file configurationFile] COMMAND COMMAND_PARAMETERS
           |  java -jar fitsVerifier.jar $COMMAND_VERIFY_FILE file
           |  java -jar fitsVerifier.jar $COMMAND_VERIFY_DIR file
           |  java -jar fitsVerifier.jar $COMMAND_VERIFY_PHYSICAL_UNIT STRING [extendedInfo]
           |  java -jar fitsVerifier.jar $COMMAND_DUMP file [output-dir] [not-apply-linear-conversion]
           |  java -jar fitsVerifier.jar $COMMAND_SLICE_IMAGE_CUBE file [output-dir] [unique-hdu]
           |  java -jar fitsVerifier.jar $COMMAND_FORMAT_TABLE_COLUMN c [display] [value] [ascii]
           | #----------------------------------------------------------------------------------------------------------
           | Example 0: Show version
           |  java -jar fitsVerifier.jar --$COMMAND_VERSION
           | #----------------------------------------------------------------------------------------------------------
           | Example 1: Verify a FITS file 'f'
           |  java -jar fitsVerifier.jar --$COMMAND_VERIFY_FILE f
           | #----------------------------------------------------------------------------------------------------------
           | Example 3: Verify a physical unit string 'm'
           |  java -jar fitsVerifier.jar --$COMMAND_VERIFY_PHYSICAL_UNIT m
           | #----------------------------------------------------------------------------------------------------------
           | Example 4: Verify a physical unit string 'm' synthesizing the extendedInfo name
           |  java -jar fitsVerifier.jar --$COMMAND_VERIFY_PHYSICAL_UNIT m -extendedInfo
           | #----------------------------------------------------------------------------------------------------------
           | Example 5: Verify a physical unit string synthesizing it with extended information
           |  java -jar fitsVerifier.jar -u "10^(46)sqrt(a/s*m/(log(a^(3/2))/log(s*m))/log(s))" -x
           | #----------------------------------------------------------------------------------------------------------
           | Example 6: verify the contents of FITS file 'f'
           |  java -jar fitsVerifier.jar --$COMMAND_VERIFY_FILE f
           |  #----------------------------------------------------------------------------------------------------------
           | Example 7: verify all FITS files of directory 'dir' recursively, generating an summary file in 'output/verify.csv'
           |  java -jar fitsVerifier.jar --$COMMAND_VERIFY_DIR dir --summary-valid-file output/valid.txt --summary-not-valid-file output/not_valid.txt
           |  #----------------------------------------------------------------------------------------------------------
           | Example 8: dump the contents of FITS file 'f' using as output the directory 'output/dump/f' and applying linear conversion
           |  java -jar fitsVerifier.jar --$COMMAND_DUMP f
           | #----------------------------------------------------------------------------------------------------------
           | Example 9: dump the contents of FITS file 'f' using as output the directory 'o' and applying linear conversion
           |  java -jar fitsVerifier.jar --$COMMAND_DUMP f --output-dir o
           | #----------------------------------------------------------------------------------------------------------
           | Example 10: dump the contents of FITS file 'f' using as output the directory 'o' but not applying linear con version
           |  java -jar fitsVerifier.jar --$COMMAND_DUMP f -o o --not-apply-linear-conversion
           | #----------------------------------------------------------------------------------------------------------
           | Example 11: slice a cube of images of FITS file 'f' into a single image sequence
           |  java -jar fitsVerifier.jar --$COMMAND_SLICE_IMAGE_CUBE f
           | #----------------------------------------------------------------------------------------------------------
           | Example 11.1: slice a cube of images of FITS file 'f' into a single image sequence creating a unique hdu.
           | if the cube is in a extension the primary header and the extension header is merged, the primary data is dropped
           | and is used the extension data instead.
           |  java -jar fitsVerifier.jar --$COMMAND_SLICE_IMAGE_CUBE f --output-dir o --unique-hdu
           | #----------------------------------------------------------------------------------------------------------
           | Example 11.2: same as 11 but slicing only the HDU in the cube with hdu-name: 'ESO_CCD_#84' and 'ESO_CCD_#00'
           |  java -jar fitsVerifier.jar --$COMMAND_SLICE_IMAGE_CUBE f --hdu-name ESO_CCD_#84 ESO_CCD_#00 --output-dir o --unique-hdu
           |  #----------------------------------------------------------------------------------------------------------
           | Example 11.3: same as 11 but slicing only the HDU at positions '7' in the cube.
           | The very first index position in the cube is 0 (main HDU)
           |  java -jar fitsVerifier.jar --$COMMAND_SLICE_IMAGE_CUBE f --slice-index 7 --output-dir o --unique-hdu
           | #----------------------------------------------------------------------------------------------------------
           | Example 11.4: same as 11 but slicing all images in directory f
           |  java -jar fitsVerifier.jar --$COMMAND_SLICE_IMAGE_CUBE f
           |  #----------------------------------------------------------------------------------------------------------
           | Example 11.5: same as 11 but slicing all images contained in file 'f': one file per line in absolute path
           |  java -jar fitsVerifier.jar --$COMMAND_SLICE_IMAGE_CUBE f --output-dir o --unique-hdu --file
           | #----------------------------------------------------------------------------------------------------------
           | Example 12: Format a binary table column value (hexadecimal sequence of bytes starting with 0x) '0xFF23' with format 'I' and display 'I8.4'
           |  java -jar fitsVerifier.jar --$COMMAND_FORMAT_TABLE_COLUMN 0xFF23 --format I --display I8.4
           | #----------------------------------------------------------------------------------------------------------
           | Example 13: Format a binary table column value sequence '0xFF23,0x3023' with format '2I' and display 'E12.4E3'
           |  java -jar fitsVerifier.jar --$COMMAND_FORMAT_TABLE_COLUMN 0xFF23,0x3023 --format 2I --display E12.4E3
           | #----------------------------------------------------------------------------------------------------------
           | Example 14: Format an ASCII table column value '5676 with format 'I3' and display 'I8.4'
           |  java -jar fitsVerifier.jar --$COMMAND_FORMAT_TABLE_COLUMN 5676 --format I3 --display I8.4 --ascii
           | #----------------------------------------------------------------------------------------------------------
           | Example 15: Format an ASCII table column value '5676 with format 'I3'
           |  java -jar fitsVerifier.jar --$COMMAND_FORMAT_TABLE_COLUMN 5676 --format I3 --ascii
           | #----------------------------------------------------------------------------------------------------------
           | Example 16: Detect the muti-extension FITS images in a directory 'dir' and report them in the file: 'f'
           | java -jar fitsVerifier.jar --$COMMAND_MEF dir --output-file f
           |  #----------------------------------------------------------------------------------------------------------
           | Example 17.1: Build a FITS file 'out' having an ASCII table using a input csv file 'csv'.
           |  The csv file has header and uses '\t' as item divider
           |  Restrictitons:
           |   1) All real numbers in the same column must have the same format: fixed point or exponential
           |   2) The column header, it is is present, can not be duplicated
           |   3) To avoid round problems, all real numbers are double. For example "2460146.40727176" es convertes to 2460146.5f
           |
           | java -jar fitsVerifier.jar --$COMMAND_BUILD_TABLE_CSV csv --output-file out --ascii --csv-has-header --csv-item-divider \t
           |   #----------------------------------------------------------------------------------------------------------
           | Example 17.2: Same as 17.1 but generating a FITS file with binary file
           |  The csv file has header and uses '\t' as item divider
           | java -jar fitsVerifier.jar --$COMMAND_BUILD_TABLE_CSV csv --output-file out --csv-has-header --csv-item-divider \t
           |  #----------------------------------------------------------------------------------------------------------
           |
           |Options:
           |""".stripMargin)
  footer("\n'fitsVerifier' ends")

  val configurationFile = opt[String](
    default = Some("input/configuration/main.conf")
    ,  short = 'c'
    ,  descr = " configuration file\n")

  val verifyFile = opt[String](
    required = false
    , short = 'f'
    , descr = "Verify a single FITS file\n"
    )

  val verifyDir = opt[String](
    required = false
    , noshort = true
    , descr = "Verify all FITS files of a dir generating a CSV\n"
  )

  val verifyPhysicalUnit = opt[String](
    required = false
    , short = 'u'
    , descr = "Verify a physical unit string\n"
  )

  val extendedInfo = opt[Boolean](
    required = false
    , short = 'x'
    , descr = "Synthesize extendedInfo when verifying a physical unit\n"
    , default = Some(false)
  )

  val version = opt[String](
    required = false
    , noshort = true
    , descr = "Program version\n"
  )

  val dump = opt[String](
    required = false
    , short = 'd'
    , descr = "Dump the content of a FITS file\n"
  )

  val outputDir = opt[String](
    required = false
    , short = 'o'
    , descr = "Output directory\n"
  )

  val outputFile = opt[String](
    required = false
    , noshort = true
    , descr = "Output file\n"
  )

  val summaryValidFile = opt[String](
    required = false
    , noshort = true
    , descr = "Summary of the valid FITS files found in a directory\n"
  )

  val summaryNotValidFile = opt[String](
    required = false
    , noshort = true
    , descr = "Summary of the not valid FITS files found in a directory\n"
  )

  val notApplyLinearConversion = opt[Boolean](
    required = false
    , noshort = true
    , default = Some(true)
    , descr = "Apply linear conversion\n"
  )

  val sliceImageCube = opt[String](
    required = false
    , short = 's'
    , descr = "Slice a cube of images in single image sequence\n"
  )

  val uniqueHdu = opt[Boolean](
    required = false
    , noshort = true
    , descr = "Merge the primary and the mage extension into a single header of a unique HDU and ignoring the primary data\n"
  )

  val formatTableColumn = opt[String](
    required = false
    , noshort = true
    , descr = "format an table column value\n"
  )

  val value = opt[String](
    required = false
    , short = 'v'
    , descr = "value of the table column that will be formatted\n"
  )

  val ascii = opt[Boolean](
    required = false
    , noshort = true
    , descr = "use a ASCII table instead of binary table\n"
  )

  val format = opt[String](
    required = false
    , noshort = true
    , descr = "format to apply an table column value\n"
  )

  val display = opt[String](
    required = false
    , noshort = true
    , descr = "display format to apply an table column value\n"
  )

  val dir = opt[Boolean](
    required = false
    , noshort = true
    , descr = "Indicated the the input is a directory, not a file\n"
  )

  val hduName = opt[List[String]](
    required = false
    , noshort = true
    , descr = "List of HDU names\n"
  )

  val sliceIndex = opt[List[Int]](
    required = false
    , noshort = true
    , descr = "List of index inside the cube. The very first index is 0\n"
  )

  val mef = opt[String](
      required = false
    , noshort = true
    , descr = "Detect and report the multi-extension FITS files\n"
  )

  val file = opt[Boolean](
    required = false
    , noshort = true
    , descr = "use a file containing a list fof files to process\n"
  )

  val buildTableCsv = opt[String](
    required = false
    , noshort = true
    , descr = "Build a FITS file from CSV file\n"
  )

  val csvHasHeader = opt[Boolean](
    required = false
    , default = Some(true)
    , noshort = true
    , descr = "use a file containing a list fof files to process\n"
  )

  val csvItemDivider = opt[String](
    required = false
    , default = Some(",")
    , noshort = true
    , descr = "use a file containing a list fof files to process\n"
  )
  //---------------------------------------------------------------------------
  validate (configurationFile) { f =>
    if (!fileExist(f) && (!directoryExist(f))) Left(s"Invalid user argument 'configurationFile'. The configuration file '$f' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate (verifyFile) { f =>
    if (!fileExist(f) ) Left(s"Invalid user argument '$COMMAND_VERIFY_FILE'. The path '$f' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate(dump) { f =>
    if (!fileExist(f)) Left(s"Invalid user argument '$COMMAND_DUMP'. The file '$f' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate(sliceImageCube) { f =>
    if ( !fileExist(f) && !directoryExist(f)) Left(s"Invalid user argument '$COMMAND_SLICE_IMAGE_CUBE'. The file or directory '$f' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------

  val commandSeq = Seq(
      verifyFile
    , verifyDir
    , verifyPhysicalUnit
    , version
    , dump
    , sliceImageCube
    , formatTableColumn
    , mef
    , buildTableCsv)

  mutuallyExclusive(commandSeq: _*)
  requireOne(commandSeq: _*)

  verify()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CommandLineParser.scala
//=============================================================================
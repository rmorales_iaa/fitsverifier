/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  31/Oct/2019
 * Time:  10h:33m
 * Description: observing manager
 */
//=============================================================================
package com.fitsVerifier
//=============================================================================
import BuildInfo.BuildInfo
import com.FitsVerifier.FitsVerifier
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import com.common.util.time.Time
import com.fitsVerifier.commandLine.CommandLineParser
//=============================================================================
object Main extends MyLogger {
  //---------------------------------------------------------------------------
  def main(args: Array[String]): Unit = {
  //-------------------------------------------------------------------------
    val userArgs = if (args.isEmpty) Array("--help") else args
    val cl = new CommandLineParser(userArgs)
    MyConf.c = MyConf(cl.configurationFile())
    if (MyConf.c == null || !MyConf.c.isLoaded)
      fatal(s"Error. Error parsing configuration file '${cl.configurationFile()}'")
    else {
      info(s"----------------------------- Scala FITS verifier '${BuildInfo.version}' starts ----------------------------")
      val startMs = System.currentTimeMillis
      FitsVerifier().run(cl)
      info(s"Scala FITS verifier elapsed time: ${Time.getFormattedElapseTimeFromStart(startMs)}")
      info(s"----------------------------- Scala FITS verifier '${BuildInfo.version}' ends ------------------------------")
      System.exit(0)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Main.scala
//=============================================================================

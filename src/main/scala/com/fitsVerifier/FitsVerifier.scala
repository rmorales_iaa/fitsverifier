/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  18/Nov/2022
 * Time:  14h:57m
 * Description: None
 */
//=============================================================================
package com.FitsVerifier
//=============================================================================
import com.common.fits.standard.ConformingVersion
import com.common.fits.standard.fits.{FitsDump, FitsSliceCube, FitsVerify}
import com.common.fits.standard.structure.hdu.extension.conforming.standard.commonTable.unit.parser.ParserPhysicalUnit
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.path.Path
import com.common.fits.standard.fits.factory.table.{FitsBuildTable, FitsFormatTableColumnValue}
import com.fitsVerifier.Version
import com.fitsVerifier.commandLine.CommandLineParser
import com.fitsVerifier.mef.MefManager
//=============================================================================
import java.math.BigInteger
//=============================================================================
object FitsVerifier {
}
//=============================================================================
case class FitsVerifier() extends App with MyLogger {
  //---------------------------------------------------------------------------
  var startMs : Long = System.currentTimeMillis
  //---------------------------------------------------------------------------
  private def initialActions(): Unit = {
    startMs = System.currentTimeMillis
  }
  //---------------------------------------------------------------------------
  private def finalActions(): Unit = {

  }
  //---------------------------------------------------------------------------
  private def commandVerifyPhysicalUnit(cl: CommandLineParser): Boolean = {
    info(s"\tVerify a physical unit string")
    val s = cl.verifyPhysicalUnit()
    if (s.isEmpty) {
      error("Can not verify the physical unit in an empty string")
      return false
    }

    val (expr, errorMessage) = ParserPhysicalUnit.parseInput(s)
    if (expr.isEmpty) {
      error(s"The string '$s' is NOT a valid physical unit as stated in: ${ConformingVersion.FITS_CONFORMING_VERSION}")
      println(errorMessage)
    }
    else {
      warning(s"The string '$s' is a valid physical unit as stated in: ${ConformingVersion.FITS_CONFORMING_VERSION}")
      println(s"Synthesized string:    '${expr.get.synthesizeString()}'")
      if (cl.extendedInfo.isSupplied) {
        println(s"Synthesized extended info: '${expr.get.synthesizeMagnitude()}'")
        println(s"Synthesized object       : '${expr.get}'")
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  private def commandVerifyFile(cl: CommandLineParser): Boolean = {
    info(s"\tVerify a single FITS file")
    val path = cl.verifyFile()
    info(s"\t\tVerifying file: '$path'")
    if (!MyFile.fileExist(path)) error(s"File: '$path' does not exist")
    else FitsVerify.verifyFile(path)
    true
  }
  //---------------------------------------------------------------------------
  private def commandVerifyDir(cl: CommandLineParser): Boolean = {
    info(s"\tVerify recursively all FITS of a directory")
    val path = cl.verifyDir()
    val validFile = cl.summaryValidFile()
    val notValidFile = cl.summaryNotValidFile()
    info(s"\t\tVerifying directory       : '$path'")
    info(s"\t\tSummary of valid files    : '$validFile'")
    info(s"\t\tSummary of not valid files: '$notValidFile'")
    if (!Path.directoryExist(path)) error(s"Directory: '$path' does not exist")
    else FitsVerify.verifyDirectory(path, validFile, notValidFile)
    true
  }
  //---------------------------------------------------------------------------
  private def commandDump(cl: CommandLineParser): Boolean = {
    val DEFAULT_OUTPUT_PATH= "output/dump/"

    info("\tDump the contents of a single FITS file")
    val path = cl.dump()
    val outputDir =
      if (cl.outputDir.isSupplied) cl.outputDir()
      else s"$DEFAULT_OUTPUT_PATH${Path.getOnlyFilenameNoExtension(path)}"
    val applyLinearConversion = !cl.notApplyLinearConversion.isSupplied

    info(s"\t\tVerifying path         : '$path'")
    info(s"\t\tOutput directory       : '$outputDir'")
    info(s"\t\tApply linear conversion: '$applyLinearConversion'")

    if (MyFile.fileExist(path))
      FitsDump.dump(path, Path.resetDirectory(outputDir), applyLinearConversion)

    true
  }
  //---------------------------------------------------------------------------
  private def commandSliceImageCube(cl: CommandLineParser): Boolean = {
    val DEFAULT_OUTPUT_PATH = "output/slice/"

    info("\tSlice a cube of images into single FITS files")
    val path = cl.sliceImageCube()
    val outputDir =
      if (cl.outputDir.isSupplied) cl.outputDir()
      else s"$DEFAULT_OUTPUT_PATH${Path.getOnlyFilenameNoExtension(path)}"
    val uniqueHdu = cl.uniqueHdu.isSupplied

    val hduNameSeq = if (cl.hduName.isSupplied) Some(cl.hduName()) else None
    val sliceIndexSeq = if (cl.sliceIndex.isSupplied) Some(cl.sliceIndex()) else None
    val processFileSet = cl.file.isSupplied

    if (!processFileSet) info(s"\t\tVerifying path         : '$path'")
    else  info(s"\t\tProcessing file set    : '$path'")

    info(s"\t\tOutput directory       : '$outputDir'")
    info(s"\t\tUnique HDU:            : '$uniqueHdu'")

    if (hduNameSeq.isDefined)
      info(s"\t\tHDU name filter:       : '${hduNameSeq.mkString(",")}'")

    if (sliceIndexSeq.isDefined)
      info(s"\t\tCube slice index filter: '${sliceIndexSeq.mkString(",")}'")

    FitsSliceCube.slice(
          path
        , Path.resetDirectory(outputDir)
        , uniqueHdu
        , hduNameSeq
        , sliceIndexSeq
        , processFileSet)

   true
  }
  //---------------------------------------------------------------------------
  private def commandFormatTableColum(cl: CommandLineParser): Boolean = {

    info("\tFormat an table column value")
    val value         = cl.formatTableColumn()
    val format        = cl.format()
    val displayFormat = if (!cl.display.isSupplied) "" else cl.display()
    val isAsciiTable  = cl.ascii.isSupplied

    info(s"\t\tValue                     : '$value'")
    info(s"\t\tFormat                    : '$format'")
    if (isAsciiTable) info(s"\t\tUsing ASII table          : 'true'")
    else info(s"\t\tUsing binary table        : 'true'")

    if (displayFormat.isEmpty) info(s"\t\tUsing display format      : '$displayFormat'")
    else info(s"\t\tNot using display format")

    if(format.isEmpty) {
      warning("Please, specify a format")
      return false
    }

    if (isAsciiTable) FitsFormatTableColumnValue.format(format, value, displayFormat, isAsciiTable)
    else {
      if (!value.startsWith("0x")) return error("The sequence of bytes must start with '0x'")
      val byteSeq = value.split(",").map { byteSeq =>
        if ((byteSeq.size % 2) != 0) return error(s"The size of sequence of bytes: '$byteSeq' must be even")
        (byteSeq.drop(2).grouped(2) map {b=>
          new BigInteger(b, 16).toByteArray.last}).toArray
      }.flatten
      FitsFormatTableColumnValue.format(format, value, displayFormat, isAsciiTable, byteSeq)
    }
    true
  }
  //---------------------------------------------------------------------------
  private def commandVersion(cl: CommandLineParser): Boolean = {
    println(Version.value)
    true
  }

  //---------------------------------------------------------------------------
  private def commandMef(cl: CommandLineParser): Boolean = {

    info("\tDetect and report multi-extension FITS files")
    val dir = cl.mef()
    val outputFile = cl.outputFile()

    info(s"\t\tDirectory                 : '$dir'")
    info(s"\t\tFormat                    : '$outputFile'")
    MefManager.detectAndReport(dir, outputFile)
    true
  }

  //---------------------------------------------------------------------------
  private def commandBuildTableCsv(cl: CommandLineParser): Boolean = {

    info("\tBuild a FITS file from csv file")
    val csvFile = cl.buildTableCsv()
    val outputFitsFile = cl.outputFile()
    val isAsciiTable = cl.ascii.isSupplied
    val csvHasHeader = cl.csvHasHeader.isSupplied
    val csvItemDivider = cl.csvItemDivider()

    info(s"\t\tCsv file                  : '$csvFile'")
    info(s"\t\tCsv has header            : '$csvHasHeader'")
    info(s"\t\tCsv item divider          : '$csvItemDivider'")
    info(s"\t\tFITS has ASCII table      : '$isAsciiTable'")
    info(s"\t\tFITS output file          : '$outputFitsFile'")
    FitsBuildTable.build(csvFile
                         , csvHasHeader
                         , csvItemDivider
                         , isAsciiTable
                         , outputFitsFile)
    true
  }
  //---------------------------------------------------------------------------
  def commandManager(cl: CommandLineParser): Boolean = {

    //astrometry
    if (cl.version.isSupplied)            return commandVersion(cl)
    if (cl.verifyFile.isSupplied)         return commandVerifyFile(cl)
    if (cl.verifyDir.isSupplied)          return commandVerifyDir(cl)
    if (cl.verifyPhysicalUnit.isSupplied) return commandVerifyPhysicalUnit(cl)
    if (cl.dump.isSupplied)               return commandDump(cl)
    if (cl.sliceImageCube.isSupplied)     return commandSliceImageCube(cl)
    if (cl.formatTableColumn.isSupplied)  return commandFormatTableColum(cl)
    if (cl.mef.isSupplied)                return commandMef(cl)
    if (cl.buildTableCsv.isSupplied)      return commandBuildTableCsv(cl)
    error(s"Unknown command")
  }
  //--------------------------------------------------------------------------
  def run(cl: CommandLineParser): Unit = {

    initialActions()
    commandManager(cl)
    finalActions()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsVerifier.scala
//=============================================================================

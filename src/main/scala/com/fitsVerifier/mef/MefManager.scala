/**
  * Created by: Rafael Morales (rmorales@iaa.es)
  * Date:  16/Jun/2023
  * Time:  23h:02m
  * Description: None
  */
package com.fitsVerifier.mef
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.standard.fits.{Fits, FitsLoad}
import com.common.hardware.cpu.CPU
import com.common.logger.MyLogger
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
//=============================================================================
import java.io.{File}
import java.io.{BufferedWriter, FileWriter}
import scala.util.{Try,Success,Failure}
import java.util.concurrent.atomic.AtomicLong
//=============================================================================
object MefManager extends MyLogger {
  //---------------------------------------------------------------------------
  private val fitsExtensionSeq = MyConf.c.getStringSeq("Common.fitsFileExtension")
  private val mefCount: AtomicLong = new AtomicLong(0)
  //---------------------------------------------------------------------------
  private class parseFileSeq(pathSeq: Array[String]
                             , bw: BufferedWriter) extends ParallelTask[String](
    pathSeq
    , CPU.getCoreCount()
    , isItemProcessingThreadSafe = true
    , randomStartMaxMsWait = 100) {
    //-------------------------------------------------------------------------
    def userProcessSingleItem(path: String) = {
      var fits : Option[Fits] = None
      Try {
        fits = FitsLoad.load(
          path
          , readDataBlockFlag = true
          , printDisconfFlag = false
         , ignoreDisconf = true)
      }
      match {
        case Success(_) =>
          if (fits.isDefined) {
            if (fits.get.isMEF()) {
              info(s"Found multi-extension file:'$path'")
              mefCount.addAndGet(1)
              synchronized { bw.write(path + "\n") }
            }
          }
          else error(s"Error processing FITS file: '$path'")

        case Failure(e) =>
          error(s"Error processing FITS file: '$path'")
          error(e.toString)
          error(e.getStackTrace.mkString("\n"))
      }
    }
    //-----------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def detectAndReport(dir: String, outputFile: String): Boolean= {

    if(!Path.directoryExist(dir)) return error(s"Error, the directory:'$dir' does not exist")
    val fileSeq = Path.getSortedFileListRecursive(dir,fitsExtensionSeq) map (_.getAbsolutePath)

    val bw = new BufferedWriter(new FileWriter(new File(outputFile)))
    new parseFileSeq(fileSeq.toArray,bw)
    bw.close()

    warning(s"Detected:'${mefCount.get()}' mef files in:'$dir'")
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file MefManager.scala
//=============================================================================
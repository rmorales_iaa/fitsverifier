#!/bin/bash
#------------------------------------
#update version
VERSION=v0.1.5
MESSAGE="Fix keyword order in 'slice-image-cube' when merge headers"
#------------------------------------
git tag -a $VERSION -m '$MESSAGE'
git push origin $VERSION
./makeFatJar
./myGit $MESSAGE
cd deploy/git_compiled_versions/scalaFitsVerifier/
git tag -a $VERSION -m '$MESSAGE'
git push origin $VERSION
./myGit $MESSAGE
cd ..
cd ..
cd ..
#------------------------------------

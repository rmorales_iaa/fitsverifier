#!/bin/bash
#-----------------------------------------------
#rebuild the database where the solved images are: ROOT_DIR
ROOT_DIR=/mnt/uxmal_groups/image_storage/dss16_disk3/observaciones/
OUTPUT_DIR=output/
#-----------------------------------------------
#set -x	  #enable debug
#-----------------------------------------------
for YEAR in {2000..2050}
do
  dir=$ROOT_DIR/$YEAR
  if [ -d $dir ]
   then
     echo "Parsing directory: '$dir'"
     java -jar fitsVerifier.jar --mef $dir --output-file $OUTPUT_DIR/$YEAR.txt
  fi
done
#-----------------------------------------------
